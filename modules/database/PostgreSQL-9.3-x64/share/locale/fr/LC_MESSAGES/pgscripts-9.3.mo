��    �      �  �   �      �  K   �     �  
   �  >     >   E  =   �  -   �  C   �  A   4     v     �  #   �     �  (   �     	  I   &  E   p     �  >   6  ;   u  =   �  :   �  <   *  9   g  6   �  5   �  C     C   R  9   �  4   �  E     =   K  .   �  ;   �  E   �  :   :  ?   u  A   �  9   �  7   1  4   i  L   �  J   �  3   6  H   j  E   �  B   �  A   <  5   ~  2   �  C   �  7   +  2   c  2   �  J   �  :     5   O  G   �  0   �  <   �  0   ;  )   l  M   �  J   �  G   /  4   w  H   �  E   �  D   ;  =   �  v   �  <   5   �   r   I   �   @   @!  5   �!  4   �!  1   �!  0   "  ;   O"  5   �"  '   �"  6   �"  3    #  4   T#  @   �#  ;   �#  ;   $  `   B$  8   �$  2   �$  9   %  6   I%  >   �%     �%  /   �%  <   �%  #   8&  #   \&  ?   �&  8   �&  %   �&  #   '     C'  3   c'  &   �'     �'  E   �'  6   (  E   B(  F   �(  K   �(  7   )  J   S)  6   �)  @   �)  >   *  D   U*  5   �*     �*  *   �*  8   +  6   R+  %   �+  (   �+  $   �+  (   �+  8   &,  #   _,      �,     �,  8   �,  4   �,  $   2-     W-  ,   w-  ,   �-  (   �-  ;   �-  9   6.     p.     �.     �.  *   �.  8   �.  ,   /  8   G/  #   �/  4   �/     �/  )   �/  7    0     X0     f0  !   {0  +   �0     �0     �0     �0     1     11     61  
   S1     ^1     w1     �1  '   �1  "   �1  2   �1  7   '2     _2  &   h2     �2     �2  /   �2     �2     �2     �2     �2     �2     3     "3     +3     -3  �  13  P   �4     #5     <5  N   H5  J   �5  I   �5  6   ,6  O   c6  M   �6     7     7  &   -7     T7  )   p7     �7  n   �7  t   %8  �   �8  B   29  B   u9  x   �9  r   1:  >   �:  <   �:  <    ;  <   ];  �   �;  N   <  @   k<  A   �<  r   �<  C   a=  C   �=  H   �=  v   2>  J   �>  U   �>  V   J?  D   �?  D   �?  E   +@     q@  �   �@  E   tA  >   �A  <   �A  <   6B  <   sB  C   �B  C   �B  w   8C  u   �C  E   &D  B   lD  �   �D  r   7E  M   �E  ~   �E  =   wF  H   �F  :   �F  ,   9G  M   fG  K   �G  L    H  ?   MH  �   �H  �   I  �   �I  >   J  �   SJ  q   �J  �   PK  r   L  q   �L  J   �L  E   BM  F   �M  F   �M  9   N  3   PN  6   �N  A   �N  @   �N  =   >O  I   |O  G   �O  E   P  �   TP  /   �P  -   Q  F   KQ  F   �Q  M   �Q     'R  8   3R  x   lR  +   �R  &   S  M   8S  D   �S  /   �S  .   �S  "   *T  <   MT  1   �T     �T  p   �T  b   5U  `   �U  c   �U  d   ]V  \   �V  g   W  `   �W  S   �W  Q   <X  n   �X  j   �X  M   hY  _   �Y  u   Z  P   �Z  7   �Z  ;   [  4   Q[  @   �[  N   �[  1   \  8   H\  9   �\  O   �\  O   ]  .   []  .   �]  >   �]  2   �]  0   +^  I   \^  G   �^     �^     _  3   ,_  E   `_  [   �_  =   `  \   @`  5   �`  H   �`  ;   a  L   Xa  c   �a     	b     b  0   ;b  >   lb     �b  $   �b  &   �b  /   c     ?c  '   Cc     kc  *   {c  )   �c     �c  2   �c  +   d  C   Ed  @   �d     �d  2   �d     e     e  7   ;e     se     ue     ye     �e     �e     �e     �e     �e     �e            �   S   �       P         �       O   Y          =   !       �      o   X   `             �   �   |   �       �          �   �   ,   �       �              �   9       L       �   3   s       @               �   �   �   8          �   :   (   E       �           ^       K              C   l         �       �      �          �       &       w   �         �   �   i   ;   �   �       �   .       Z   �   #   "   r   z   n   G   �   �   q                     ~       b   %   4   m           �   }   N   y   u   �   0   H       '                     c          j   M       �   /      A   �   �   Q   U           W   F   x   �   V   B       �          -   +   �           �   1       e           �   ]   �           R   �   �   \       g   �   5       �   T   �   t       2   d   6   k         {   7   >       �   �              $   �      )       h       *   �       
   <   D   	       �   I   p   J   a   �       ?   v   _   �       [       �   �   f          �    
By default, a database with the same name as the current user is created.
 
Connection options:
 
Options:
 
Read the description of the SQL command CLUSTER for details.
 
Read the description of the SQL command REINDEX for details.
 
Read the description of the SQL command VACUUM for details.
 
Report bugs to <pgsql-bugs@postgresql.org>.
       --lc-collate=LOCALE      LC_COLLATE setting for the database
       --lc-ctype=LOCALE        LC_CTYPE setting for the database
   %s [OPTION]...
   %s [OPTION]... DBNAME
   %s [OPTION]... LANGNAME [DBNAME]
   %s [OPTION]... [DBNAME]
   %s [OPTION]... [DBNAME] [DESCRIPTION]
   %s [OPTION]... [ROLENAME]
   --if-exists               don't report error if database doesn't exist
   --if-exists               don't report error if user doesn't exist
   --interactive             prompt for missing role name and attributes rather
                            than using defaults
   --maintenance-db=DBNAME      alternate maintenance database
   --maintenance-db=DBNAME   alternate maintenance database
   --no-replication          role cannot initiate replication
   --replication             role can initiate replication
   -?, --help                      show this help, then exit
   -?, --help                   show this help, then exit
   -?, --help                show this help, then exit
   -?, --help               show this help, then exit
   -D, --no-createdb         role cannot create databases (default)
   -D, --tablespace=TABLESPACE  default tablespace for the database
   -E, --encoding=ENCODING      encoding for the database
   -E, --encrypted           encrypt stored password
   -F, --freeze                    freeze row transaction information
   -I, --no-inherit          role does not inherit privileges
   -L, --no-login            role cannot login
   -N, --unencrypted         do not encrypt stored password
   -O, --owner=OWNER            database user to own the new database
   -P, --pwprompt            assign a password to new role
   -R, --no-createrole       role cannot create roles (default)
   -S, --no-superuser        role will not be superuser (default)
   -T, --template=TEMPLATE      template database to copy
   -U, --username=USERNAME      user name to connect as
   -U, --username=USERNAME   user name to connect as
   -U, --username=USERNAME   user name to connect as (not the one to create)
   -U, --username=USERNAME   user name to connect as (not the one to drop)
   -U, --username=USERNAME  user name to connect as
   -V, --version                   output version information, then exit
   -V, --version                output version information, then exit
   -V, --version             output version information, then exit
   -V, --version            output version information, then exit
   -W, --password               force password prompt
   -W, --password            force password prompt
   -Z, --analyze-only              only update optimizer statistics
   -a, --all                       vacuum all databases
   -a, --all                 cluster all databases
   -a, --all                 reindex all databases
   -c, --connection-limit=N  connection limit for role (default: no limit)
   -d, --createdb            role can create new databases
   -d, --dbname=DBNAME             database to vacuum
   -d, --dbname=DBNAME       database from which to remove the language
   -d, --dbname=DBNAME       database to cluster
   -d, --dbname=DBNAME       database to install language in
   -d, --dbname=DBNAME       database to reindex
   -d, --dbname=DBNAME      database name
   -e, --echo                      show the commands being sent to the server
   -e, --echo                   show the commands being sent to the server
   -e, --echo                show the commands being sent to the server
   -f, --full                      do full vacuuming
   -h, --host=HOSTNAME          database server host or socket directory
   -h, --host=HOSTNAME       database server host or socket directory
   -h, --host=HOSTNAME      database server host or socket directory
   -i, --index=INDEX         recreate specific index(es) only
   -i, --inherit             role inherits privileges of roles it is a
                            member of (default)
   -i, --interactive         prompt before deleting anything
   -i, --interactive         prompt before deleting anything, and prompt for
                            role name if not specified
   -l, --list                show a list of currently installed languages
   -l, --locale=LOCALE          locale settings for the database
   -l, --login               role can login (default)
   -p, --port=PORT              database server port
   -p, --port=PORT           database server port
   -p, --port=PORT          database server port
   -q, --quiet                     don't write any messages
   -q, --quiet               don't write any messages
   -q, --quiet              run quietly
   -r, --createrole          role can create new roles
   -s, --superuser           role will be superuser
   -s, --system              reindex system catalogs
   -t, --table='TABLE[(COLUMNS)]'  vacuum specific table(s) only
   -t, --table=TABLE         cluster specific table(s) only
   -t, --table=TABLE         reindex specific table(s) only
   -t, --timeout=SECS       seconds to wait when attempting connection, 0 disables (default: %s)
   -v, --verbose                   write a lot of output
   -v, --verbose             write a lot of output
   -w, --no-password            never prompt for password
   -w, --no-password         never prompt for password
   -z, --analyze                   update optimizer statistics
 %s (%s/%s)  %s cleans and analyzes a PostgreSQL database.

 %s clusters all previously clustered tables in a database.

 %s creates a PostgreSQL database.

 %s creates a new PostgreSQL role.

 %s installs a procedural language into a PostgreSQL database.

 %s issues a connection check to a PostgreSQL database.

 %s reindexes a PostgreSQL database.

 %s removes a PostgreSQL database.

 %s removes a PostgreSQL role.

 %s removes a procedural language from a database.

 %s: "%s" is not a valid encoding name
 %s: %s %s: cannot cluster all databases and a specific one at the same time
 %s: cannot cluster specific table(s) in all databases
 %s: cannot reindex all databases and a specific one at the same time
 %s: cannot reindex all databases and system catalogs at the same time
 %s: cannot reindex specific index(es) and system catalogs at the same time
 %s: cannot reindex specific index(es) in all databases
 %s: cannot reindex specific table(s) and system catalogs at the same time
 %s: cannot reindex specific table(s) in all databases
 %s: cannot use the "freeze" option when performing only analyze
 %s: cannot use the "full" option when performing only analyze
 %s: cannot vacuum all databases and a specific one at the same time
 %s: cannot vacuum specific table(s) in all databases
 %s: clustering database "%s"
 %s: clustering of database "%s" failed: %s %s: clustering of table "%s" in database "%s" failed: %s %s: comment creation failed (database was created): %s %s: could not connect to database %s
 %s: could not connect to database %s: %s %s: could not fetch default options
 %s: could not get current user name: %s
 %s: could not obtain information about current user: %s
 %s: creation of new role failed: %s %s: database creation failed: %s %s: database removal failed: %s %s: language "%s" is already installed in database "%s"
 %s: language "%s" is not installed in database "%s"
 %s: language installation failed: %s %s: language removal failed: %s %s: missing required argument database name
 %s: missing required argument language name
 %s: missing required argument role name
 %s: only one of --locale and --lc-collate can be specified
 %s: only one of --locale and --lc-ctype can be specified
 %s: query failed: %s %s: query was: %s
 %s: reindexing database "%s"
 %s: reindexing of database "%s" failed: %s %s: reindexing of index "%s" in database "%s" failed: %s %s: reindexing of system catalogs failed: %s %s: reindexing of table "%s" in database "%s" failed: %s %s: removal of role "%s" failed: %s %s: too many command-line arguments (first is "%s")
 %s: vacuuming database "%s"
 %s: vacuuming of database "%s" failed: %s %s: vacuuming of table "%s" in database "%s" failed: %s Are you sure? Cancel request sent
 Could not send cancel request: %s Database "%s" will be permanently removed.
 Enter it again:  Enter name of role to add:  Enter name of role to drop:  Enter password for new role:  Name Password encryption failed.
 Password:  Passwords didn't match.
 Please answer "%s" or "%s".
 Procedural Languages Role "%s" will be permanently removed.
 Shall the new role be a superuser? Shall the new role be allowed to create databases? Shall the new role be allowed to create more new roles? Trusted? Try "%s --help" for more information.
 Usage:
 accepting connections
 cannot duplicate null pointer (internal error)
 n no no attempt
 no response
 out of memory
 rejecting connections
 unknown
 y yes Project-Id-Version: PostgreSQL 9.3
Report-Msgid-Bugs-To: pgsql-bugs@postgresql.org
POT-Creation-Date: 2014-12-02 20:17+0000
PO-Revision-Date: 2014-12-05 09:57+0100
Last-Translator: Guillaume Lelarge <guillaume@lelarge.info>
Language-Team: PostgreSQLfr <pgsql-fr-generale@postgresql.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.5.4
 
Par défaut, la base de donnée créée porte le nom de l'utilisateur courant.
 
Options de connexion :
 
Options :
 
Lire la description de la commande SQL CLUSTER pour de plus amples détails.
 
Lire la description de la commande SQL REINDEX pour plus d'informations.
 
Lire la description de la commande SQL VACUUM pour plus d'informations.
 
Rapporter les bogues à <pgsql-bugs@postgresql.org>.
       --lc-collate=LOCALE       paramètre LC_COLLATE pour la base de données
       --lc-ctype=LOCALE         paramètre LC_CTYPE pour la base de données
   %s [OPTION]...
   %s [OPTION]... NOMBASE
   %s [OPTION]... NOMLANGAGE [NOMBASE]
   %s [OPTION]... [NOMBASE]
   %s [OPTION]... [NOMBASE] [DESCRIPTION]
   %s [OPTION]... [NOMROLE]
   --if-exists                  ne renvoie pas d'erreur si la base
                               n'existe pas
   --if-exists                  ne renvoie pas d'erreur si l'utilisateur
                               n'existe pas
   --interactive                  demande le nom du rôle et les attributs
                                 plutôt qu'utiliser des valeurs par défaut
   --maintenance-db=NOM_BASE    indique une autre base par défaut
   --maintenance-db=NOM_BASE    indique une autre base par défaut
   --no-replication             le rôle ne peut pas initier de connexion de
                               réplication
   --replication                le rôle peut initier une connexion de
                               réplication
   -?, --help                   affiche cette aide puis quitte
   -?, --help                 affiche cette aide puis quitte
   -?, --help                 affiche cette aide puis quitte
   -?, --help                 affiche cette aide puis quitte
   -D, --no-createdb              le rôle ne peut pas créer de bases de
                                 données (par défaut)
   -D, --tablespace=TABLESPACE   tablespace par défaut de la base de données
   -E, --encoding=ENC            encodage de la base de données
   -E, --encrypted                chiffre le mot de passe stocké
   -F, --freeze                  gèle les informations de transactions des
                                lignes
   -I, --no-inherit               le rôle n'hérite pas des droits
   -L, --no-login                 le rôle ne peut pas se connecter
   -N, --unencrypted              ne chiffre pas le mot de passe stocké
   -O, --owner=PROPRIÉTAIRE      nom du propriétaire de la nouvelle base de
                                données
   -P, --pwprompt                 affecte un mot de passe au nouveau rôle
   -R, --no-createrole            le rôle ne peut pas créer de rôles (par défaut)
   -S, --no-superuser             le rôle ne sera pas super-utilisateur (par défaut)
   -T, --template=MODÈLE         base de données modèle à copier
   -U, --username=NOMUTILISATEUR nom d'utilisateur pour la connexion
   -U, --username=NOMUTILISATEUR  nom d'utilisateur pour la connexion
   -U, --username=NOMUTILISATEUR  nom de l'utilisateur pour la connexion (pas
                                 celui à créer)
   -U, --username=NOMUTILISATEUR  nom de l'utilisateur pour la connexion (pas
                                 celui à supprimer)
   -U, --username=NOMUTILISATEUR  nom d'utilisateur pour la connexion
   -V, --version                affiche la version puis quitte
   -V, --version              affiche la version puis quitte
   -V, --version              affiche la version puis quitte
   -V, --version              affiche la version puis quitte
   -W, --password                force la demande d'un mot de passe
   -W, --password                force la demande d'un mot de passe
   -Z, --analyze-only            met seulement à jour les statistiques de
                                l'optimiseur
   -a, --all                       exécute VACUUM sur toutes les bases de
                                  données
   -a, --all                 réorganise toutes les bases de données
   -a, --all                réindexe toutes les bases de données
   -c, --connection-limit=N       nombre maximal de connexions pour le rôle
                                 (par défaut sans limite)
   -d, --createdb                 l'utilisateur peut créer des bases de
                                 données
   -d, --dbname=NOMBASE            exécute VACUUM sur cette base de données
   -d, --dbname=NOMBASE           base de données à partir de laquelle
                                 supprimer le langage
   -d, --dbname=NOMBASE      base de données à réorganiser
   -d, --dbname=NOMBASE           base sur laquelle installer le langage
   -d, --dbname=NOMBASE     base de données à réindexer
   -d, --dbname=NOMBASE     base de données
   -e, --echo                      affiche les commandes envoyées au serveur
   -e, --echo                    affiche les commandes envoyées au serveur
   -e, --echo                     affiche les commandes envoyées au serveur
   -f, --full                      exécute VACUUM en mode FULL
   -h, --host=HOTE               hôte du serveur de bases de données
                                ou répertoire des sockets
   -h, --host=HOTE                hôte du serveur de bases de données ou
                                 répertoire des sockets
   -h, --host=NOMHÔTE           hôte du serveur de bases de données ou
                               répertoire des sockets
   -i, --index=INDEX        recrée uniquement cet (ces) index
   -i, --inherit                  le rôle hérite des droits des rôles dont il
                                 est membre (par défaut)
   -i, --interactive         demande confirmation avant de supprimer quoi que
                            ce soit
   -i, --interactive         demande confirmation avant de supprimer quoi que
                            ce soit, et demande le nom du rôle s'il n'est pas
                            indiqué
   -l, --list                     affiche la liste des langages déjà
                                 installés
   -l, --locale=LOCALE           paramètre de la locale pour la base de
                                données
   -l, --login                    le rôle peut se connecter (par défaut)
   -p, --port=PORT               port du serveur de bases de données
   -p, --port=PORT                port du serveur de bases de données
   -p, --port=PORT                port du serveur de bases de données
   -q, --quiet                     n'écrit aucun message
   -q, --quiet               n'écrit aucun message
   -q, --quiet               s'exécute sans affichage
   -r, --createrole               le rôle peut créer des rôles
   -s, --superuser                le rôle est super-utilisateur
   -s, --system             réindexe les catalogues système
   -t, --table='TABLE[(COLONNES)]' exécute VACUUM sur cette (ces) tables
   -t, --table=TABLE         réorganise uniquement cette(ces) table(s)
   -t, --table=TABLE        réindexe uniquement cette (ces) table(s)
   -t, --timeout=SECS       durée en secondes à attendre lors d'une tentative de connexion
                           0 pour désactiver (défaut: %s)
   -v, --verbose                   mode verbeux
   -v, --verbose                 mode verbeux
   -w, --no-password             empêche la demande d'un mot de passe
   -w, --no-password             empêche la demande d'un mot de passe
   -z, --analyze                 met à jour les statistiques de l'optimiseur
 %s (%s/%s)  %s nettoie et analyse une base de données PostgreSQL.

 %s réorganise toutes les tables précédemment réorganisées au sein d'une base
de données via la commande CLUSTER.

 %s crée une base de données PostgreSQL.

 %s crée un nouvel rôle PostgreSQL.

 %s installe un langage de procédures dans une base de données PostgreSQL.

 %s produitun test de connexion à une base de données PostgreSQL.

 %s réindexe une base de données PostgreSQL.

 %s supprime une base de données PostgreSQL.

 %s supprime un rôle PostgreSQL.

 %s supprime un langage procédural d'une base de données.

 %s : « %s » n'est pas un nom d'encodage valide
 %s : %s %s : ne réorganise pas à la fois toutes les bases de données et une base
spécifique via la commande CLUSTER
 %s : impossible de réorganiser la(les) table(s) spécifique(s) dans toutes les bases de données
 %s : ne peut pas réindexer toutes les bases de données et une base
spécifique en même temps
 %s : ne peut pas réindexer toutes les bases de données et les catalogues
système en même temps
 %s : ne peut pas réindexer un (des) index spécifique(s) et
les catalogues système en même temps
 %s : ne peut pas réindexer un (des) index spécifique(s) dans toutes les
bases de données
 %s : ne peut pas réindexer une (des) table(s) spécifique(s) etles catalogues système en même temps
 %s : ne peut pas réindexer une (des) table(s) spécifique(s) dans toutes
les bases de données
 %s : ne peut utiliser l'option « freeze » lors de l'exécution d'un ANALYZE
seul
 %s : ne peut utiliser l'option « full » lors de l'exécution d'un ANALYZE seul
 %s : ne peut pas exécuter VACUUM sur toutes les bases de données et sur une
base spécifique en même temps
 %s : ne peut pas exécuter VACUUM sur une(des)  table(s) spécifique(s)
dans toutes les bases de données
 %s : réorganisation de la base de données « %s » via la commande CLUSTER
 %s : la réorganisation de la base de données « %s » via la commande
CLUSTER a échoué : %s %s : la réorganisation de la table « %s » de la base de données « %s » avec
la commande CLUSTER a échoué : %s %s: l'ajout du commentaire a échoué (la base de données a été créée) : %s %s : n'a pas pu se connecter à la base de données %s
 %s : n'a pas pu se connecter à la base de données %s : %s %s : n'a pas pu récupérer les options par défaut
 %s : n'a pas pu récupérer le nom de l'utilisateur actuel : %s
 %s : n'a pas pu obtenir les informations concernant l'utilisateur actuel : %s
 %s : la création du nouvel rôle a échoué : %s %s : la création de la base de données a échoué : %s %s: la suppression de la base de données a échoué : %s %s : le langage « %s » est déjà installé sur la base de données « %s »
 %s : le langage « %s » n'est pas installé dans la base de données « %s »
 %s : l'installation du langage a échoué : %s %s : la suppression du langage a échoué : %s %s : argument nom de la base de données requis mais manquant
 %s : argument nom du langage requis mais manquant
 %s : argument nom du rôle requis mais manquant
 %s : une seule des options --locale et --lc-collate peut être indiquée
 %s : une seule des options --locale et --lc-ctype peut être indiquée
 %s : échec de la requête : %s %s : la requête était : %s
 %s : réindexation de la base de données « %s »
 %s : la réindexation de la base de données « %s » a échoué : %s %s : la réindexation de l'index « %s » dans la base de données « %s » a
échoué : %s %s : la réindexation des catalogues système a échoué : %s %s : la réindexation de la table « %s » dans la base de données « %s » a
échoué : %s %s : la suppression du rôle « %s » a échoué : %s %s : trop d'arguments en ligne de commande (le premier étant « %s »)
 %s : exécution de VACUUM sur la base de données « %s »
 %s : l'exécution de VACUUM sur la base de données « %s » a échoué : %s %s : l'exécution de VACUUM sur la table « %s » dans la base de données
« %s » a échoué : %s Êtes-vous sûr ? Requête d'annulation envoyée
 N'a pas pu envoyer la requête d'annulation : %s La base de données « %s » sera définitivement supprimée.
 Le saisir de nouveau :  Saisir le nom du rôle à ajouter :  Saisir le nom du rôle à supprimer :  Saisir le mot de passe pour le nouveau rôle :  Nom Échec du chiffrement du mot de passe.
 Mot de passe :  Les mots de passe ne sont pas identiques.
 Merci de répondre « %s » ou « %s ».
 Langages procéduraux Le rôle « %s » sera définitivement supprimé.
 Le nouveau rôle est-il super-utilisateur ? Le nouveau rôle est-il autorisé à créer des bases de données ? Le nouveau rôle est-il autorisé à créer de nouveaux rôles ? De confiance (trusted) ? Essayer « %s --help » pour plus d'informations.
 Usage :
 acceptation des connexions
 ne peut pas dupliquer un pointeur nul (erreur interne)
 n non pas de tentative
 pas de réponse
 mémoire épuisée
 rejet des connexions
 inconnu
 o oui 