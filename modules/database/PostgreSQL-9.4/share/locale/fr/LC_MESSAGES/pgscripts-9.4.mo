��    �                  K        ]  
   s  >   ~  >   �  =   �  -   :  �   h  C   �  A   9     {     �  #   �     �  (   �       I   +  E   u     �  >   ;  ;   z  =   �  :   �  <   /  9   l  6   �  5   �  C     C   W  9   �  4   �  E   
  =   P  .   �  ;   �  E   �  :   ?  ?   z  A   �  9   �  7   6  4   n  L   �  J   �  3   ;  H   o  E   �  B   �  A   A  5   �  2   �  C   �  7   0  2   h  2   �  J   �  :     5   T  G   �  0   �  <     0   @  )   q  M   �  J   �  G   4  4   |  C   �  H   �  E   >   D   �   =   �   v   !  <   ~!  �   �!  I   ?"  @   �"  5   �"  4    #  1   5#  0   g#  ;   �#  5   �#  '   
$  6   2$  3   i$  4   �$  @   �$  ;   %  ;   O%  `   �%  8   �%  2   %&  9   X&  6   �&  >   �&     '  /   '  <   D'  #   �'  #   �'  ?   �'  8   	(  %   B(  #   h(     �(  3   �(  &   �(     )  E   )  6   T)  E   �)  F   �)  K   *  7   d*  J   �*  6   �*  @   +  >   _+  D   �+  5   �+     ,  *   7,  8   b,  6   �,  %   �,  (   �,  $   !-  #   F-      j-     �-  8   �-  4   �-  $   .     >.  ,   ^.  ,   �.  (   �.  ;   �.  9   /     W/     l/     /  *   �/  8   �/  ,   0  8   .0  #   g0  4   �0     �0  )   �0  7   1     ?1     M1  !   b1  +   �1     �1     �1     �1     �1  .   2  3   G2  2   {2     �2     �2  
   �2     �2     �2     3  '   &3  "   N3  2   q3  7   �3     �3  &   �3     4     4  /   +4  +   [4     �4     �4     �4     �4     �4     �4     �4     �4  (   �4     5     5  �  5  P   �6     	7     "7  N   .7  J   }7  I   �7  6   8  �   I8  O   9  M   g9     �9     �9  &   �9     :  )   $:     N:  n   j:  t   �:  �   N;  B   �;  B   )<  x   l<  r   �<  >   X=  <   �=  <   �=  <   >  �   N>  N   �>  @   ?  A   `?  r   �?  C   @  C   Y@  H   �@  v   �@  J   ]A  U   �A  V   �A  D   UB  D   �B  E   �B     %C  �   �C  E   (D  >   nD  <   �D  <   �D  <   'E  C   dE  C   �E  w   �E  u   dF  E   �F  B    G  �   cG  r   �G  M   ^H  ~   �H  =   +I  H   iI  :   �I  ,   �I  M   J  K   hJ  L   �J  ?   K  M   AK  �   �K  �   L  �   �L  >   M  �   UM  q   �M  �   RN  r   O  q   �O  J   �O  E   DP  F   �P  F   �P  9   Q  3   RQ  6   �Q  A   �Q  @   �Q  =   @R  I   ~R  G   �R  E   S  �   VS  /   �S  -   T  F   MT  F   �T  M   �T     )U  8   5U  x   nU  +   �U  &   V  M   :V  D   �V  /   �V  .   �V  "   ,W  <   OW  1   �W     �W  p   �W  b   7X  `   �X  c   �X  d   _Y  \   �Y  g   !Z  `   �Z  S   �Z  Q   >[  n   �[  j   �[  M   j\  _   �\  u   ]  P   �]  7   �]  ;   ^  4   S^  1   �^  8   �^  9   �^  O   -_  O   }_  .   �_  .   �_  >   +`  2   j`  0   �`  I   �`  G   a     `a     �a  3   �a  E   �a  [   b  =   tb  \   �b  5   c  H   Ec  ;   �c  L   �c  c   d     {d     �d  0   �d  >   �d     e  $   5e  &   Ze  /   �e  9   �e  D   �e  D   0f     uf  '   yf     �f  *   �f  )   �f     g  2   g  +   Og  C   {g  @   �g      h  2   h     Lh     Uh  7   qh  @   �h     �h     �h     �h     i     i     'i     =i     Fi  B   ai     �i     �i     �   ^   S   L   [   {   =       
       y   `   �       g      q   %      n       �         s   �   +   �   �   �       Y   �       J   @          4   �   �   �           K                        �      "   �   0   �   N   �   b       �   	   .           /   E       x   �   �       �   j   �   �          ,   f   �       \   �   �   )   �   R   �   �   �   W           �      1      2   v       '   �   F       �           u   8   �       �   -   ~   �   �   �   G   �   �              #              !      t   r      �   _       ?           c   �   <           e                  B   M               �   6   ]      V      T       p       �       z       �       �   d   �   Z   w   5              U         P      ;   >   a       o   i   A   (   �   *       Q   �       �      m       �          l   k           �   �   O   �       �   �   �              �   I       $   �   }   �       H   h   �   �   9       3              7   �   C   &                  D   �   :      �   X   �   |        
By default, a database with the same name as the current user is created.
 
Connection options:
 
Options:
 
Read the description of the SQL command CLUSTER for details.
 
Read the description of the SQL command REINDEX for details.
 
Read the description of the SQL command VACUUM for details.
 
Report bugs to <pgsql-bugs@postgresql.org>.
       --analyze-in-stages         only update optimizer statistics, in multiple
                                  stages for faster results
       --lc-collate=LOCALE      LC_COLLATE setting for the database
       --lc-ctype=LOCALE        LC_CTYPE setting for the database
   %s [OPTION]...
   %s [OPTION]... DBNAME
   %s [OPTION]... LANGNAME [DBNAME]
   %s [OPTION]... [DBNAME]
   %s [OPTION]... [DBNAME] [DESCRIPTION]
   %s [OPTION]... [ROLENAME]
   --if-exists               don't report error if database doesn't exist
   --if-exists               don't report error if user doesn't exist
   --interactive             prompt for missing role name and attributes rather
                            than using defaults
   --maintenance-db=DBNAME      alternate maintenance database
   --maintenance-db=DBNAME   alternate maintenance database
   --no-replication          role cannot initiate replication
   --replication             role can initiate replication
   -?, --help                      show this help, then exit
   -?, --help                   show this help, then exit
   -?, --help                show this help, then exit
   -?, --help               show this help, then exit
   -D, --no-createdb         role cannot create databases (default)
   -D, --tablespace=TABLESPACE  default tablespace for the database
   -E, --encoding=ENCODING      encoding for the database
   -E, --encrypted           encrypt stored password
   -F, --freeze                    freeze row transaction information
   -I, --no-inherit          role does not inherit privileges
   -L, --no-login            role cannot login
   -N, --unencrypted         do not encrypt stored password
   -O, --owner=OWNER            database user to own the new database
   -P, --pwprompt            assign a password to new role
   -R, --no-createrole       role cannot create roles (default)
   -S, --no-superuser        role will not be superuser (default)
   -T, --template=TEMPLATE      template database to copy
   -U, --username=USERNAME      user name to connect as
   -U, --username=USERNAME   user name to connect as
   -U, --username=USERNAME   user name to connect as (not the one to create)
   -U, --username=USERNAME   user name to connect as (not the one to drop)
   -U, --username=USERNAME  user name to connect as
   -V, --version                   output version information, then exit
   -V, --version                output version information, then exit
   -V, --version             output version information, then exit
   -V, --version            output version information, then exit
   -W, --password               force password prompt
   -W, --password            force password prompt
   -Z, --analyze-only              only update optimizer statistics
   -a, --all                       vacuum all databases
   -a, --all                 cluster all databases
   -a, --all                 reindex all databases
   -c, --connection-limit=N  connection limit for role (default: no limit)
   -d, --createdb            role can create new databases
   -d, --dbname=DBNAME             database to vacuum
   -d, --dbname=DBNAME       database from which to remove the language
   -d, --dbname=DBNAME       database to cluster
   -d, --dbname=DBNAME       database to install language in
   -d, --dbname=DBNAME       database to reindex
   -d, --dbname=DBNAME      database name
   -e, --echo                      show the commands being sent to the server
   -e, --echo                   show the commands being sent to the server
   -e, --echo                show the commands being sent to the server
   -f, --full                      do full vacuuming
   -g, --role=ROLE           new role will be a member of this role
   -h, --host=HOSTNAME          database server host or socket directory
   -h, --host=HOSTNAME       database server host or socket directory
   -h, --host=HOSTNAME      database server host or socket directory
   -i, --index=INDEX         recreate specific index(es) only
   -i, --inherit             role inherits privileges of roles it is a
                            member of (default)
   -i, --interactive         prompt before deleting anything
   -i, --interactive         prompt before deleting anything, and prompt for
                            role name if not specified
   -l, --list                show a list of currently installed languages
   -l, --locale=LOCALE          locale settings for the database
   -l, --login               role can login (default)
   -p, --port=PORT              database server port
   -p, --port=PORT           database server port
   -p, --port=PORT          database server port
   -q, --quiet                     don't write any messages
   -q, --quiet               don't write any messages
   -q, --quiet              run quietly
   -r, --createrole          role can create new roles
   -s, --superuser           role will be superuser
   -s, --system              reindex system catalogs
   -t, --table='TABLE[(COLUMNS)]'  vacuum specific table(s) only
   -t, --table=TABLE         cluster specific table(s) only
   -t, --table=TABLE         reindex specific table(s) only
   -t, --timeout=SECS       seconds to wait when attempting connection, 0 disables (default: %s)
   -v, --verbose                   write a lot of output
   -v, --verbose             write a lot of output
   -w, --no-password            never prompt for password
   -w, --no-password         never prompt for password
   -z, --analyze                   update optimizer statistics
 %s (%s/%s)  %s cleans and analyzes a PostgreSQL database.

 %s clusters all previously clustered tables in a database.

 %s creates a PostgreSQL database.

 %s creates a new PostgreSQL role.

 %s installs a procedural language into a PostgreSQL database.

 %s issues a connection check to a PostgreSQL database.

 %s reindexes a PostgreSQL database.

 %s removes a PostgreSQL database.

 %s removes a PostgreSQL role.

 %s removes a procedural language from a database.

 %s: "%s" is not a valid encoding name
 %s: %s %s: cannot cluster all databases and a specific one at the same time
 %s: cannot cluster specific table(s) in all databases
 %s: cannot reindex all databases and a specific one at the same time
 %s: cannot reindex all databases and system catalogs at the same time
 %s: cannot reindex specific index(es) and system catalogs at the same time
 %s: cannot reindex specific index(es) in all databases
 %s: cannot reindex specific table(s) and system catalogs at the same time
 %s: cannot reindex specific table(s) in all databases
 %s: cannot use the "freeze" option when performing only analyze
 %s: cannot use the "full" option when performing only analyze
 %s: cannot vacuum all databases and a specific one at the same time
 %s: cannot vacuum specific table(s) in all databases
 %s: clustering database "%s"
 %s: clustering of database "%s" failed: %s %s: clustering of table "%s" in database "%s" failed: %s %s: comment creation failed (database was created): %s %s: could not connect to database %s
 %s: could not connect to database %s: %s %s: could not fetch default options
 %s: creation of new role failed: %s %s: database creation failed: %s %s: database removal failed: %s %s: language "%s" is already installed in database "%s"
 %s: language "%s" is not installed in database "%s"
 %s: language installation failed: %s %s: language removal failed: %s %s: missing required argument database name
 %s: missing required argument language name
 %s: missing required argument role name
 %s: only one of --locale and --lc-collate can be specified
 %s: only one of --locale and --lc-ctype can be specified
 %s: query failed: %s %s: query was: %s
 %s: reindexing database "%s"
 %s: reindexing of database "%s" failed: %s %s: reindexing of index "%s" in database "%s" failed: %s %s: reindexing of system catalogs failed: %s %s: reindexing of table "%s" in database "%s" failed: %s %s: removal of role "%s" failed: %s %s: too many command-line arguments (first is "%s")
 %s: vacuuming database "%s"
 %s: vacuuming of database "%s" failed: %s %s: vacuuming of table "%s" in database "%s" failed: %s Are you sure? Cancel request sent
 Could not send cancel request: %s Database "%s" will be permanently removed.
 Enter it again:  Enter name of role to add:  Enter name of role to drop:  Enter password for new role:  Generating default (full) optimizer statistics Generating medium optimizer statistics (10 targets) Generating minimal optimizer statistics (1 target) Name Password encryption failed.
 Password:  Passwords didn't match.
 Please answer "%s" or "%s".
 Procedural Languages Role "%s" will be permanently removed.
 Shall the new role be a superuser? Shall the new role be allowed to create databases? Shall the new role be allowed to create more new roles? Trusted? Try "%s --help" for more information.
 Usage:
 accepting connections
 cannot duplicate null pointer (internal error)
 could not look up effective user ID %ld: %s n no no attempt
 no response
 out of memory
 rejecting connections
 unknown
 user does not exist user name lookup failure: error code %lu y yes Project-Id-Version: PostgreSQL 9.4
Report-Msgid-Bugs-To: pgsql-bugs@postgresql.org
POT-Creation-Date: 2015-02-08 09:12+0000
PO-Revision-Date: 2015-02-08 11:08+0100
Last-Translator: Guillaume Lelarge <guillaume@lelarge.info>
Language-Team: PostgreSQLfr <pgsql-fr-generale@postgresql.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.7.3
 
Par défaut, la base de donnée créée porte le nom de l'utilisateur courant.
 
Options de connexion :
 
Options :
 
Lire la description de la commande SQL CLUSTER pour de plus amples détails.
 
Lire la description de la commande SQL REINDEX pour plus d'informations.
 
Lire la description de la commande SQL VACUUM pour plus d'informations.
 
Rapporter les bogues à <pgsql-bugs@postgresql.org>.
   --analyze-in-stages            met seulement à jour les statistiques de
                                 l'optimiseur, en plusieurs étapes pour de
                                 meilleurs résultats
       --lc-collate=LOCALE       paramètre LC_COLLATE pour la base de données
       --lc-ctype=LOCALE         paramètre LC_CTYPE pour la base de données
   %s [OPTION]...
   %s [OPTION]... NOMBASE
   %s [OPTION]... NOMLANGAGE [NOMBASE]
   %s [OPTION]... [NOMBASE]
   %s [OPTION]... [NOMBASE] [DESCRIPTION]
   %s [OPTION]... [NOMROLE]
   --if-exists                  ne renvoie pas d'erreur si la base
                               n'existe pas
   --if-exists                  ne renvoie pas d'erreur si l'utilisateur
                               n'existe pas
   --interactive                  demande le nom du rôle et les attributs
                                 plutôt qu'utiliser des valeurs par défaut
   --maintenance-db=NOM_BASE    indique une autre base par défaut
   --maintenance-db=NOM_BASE    indique une autre base par défaut
   --no-replication             le rôle ne peut pas initier de connexion de
                               réplication
   --replication                le rôle peut initier une connexion de
                               réplication
   -?, --help                   affiche cette aide puis quitte
   -?, --help                 affiche cette aide puis quitte
   -?, --help                 affiche cette aide puis quitte
   -?, --help                 affiche cette aide puis quitte
   -D, --no-createdb              le rôle ne peut pas créer de bases de
                                 données (par défaut)
   -D, --tablespace=TABLESPACE   tablespace par défaut de la base de données
   -E, --encoding=ENC            encodage de la base de données
   -E, --encrypted                chiffre le mot de passe stocké
   -F, --freeze                  gèle les informations de transactions des
                                lignes
   -I, --no-inherit               le rôle n'hérite pas des droits
   -L, --no-login                 le rôle ne peut pas se connecter
   -N, --unencrypted              ne chiffre pas le mot de passe stocké
   -O, --owner=PROPRIÉTAIRE      nom du propriétaire de la nouvelle base de
                                données
   -P, --pwprompt                 affecte un mot de passe au nouveau rôle
   -R, --no-createrole            le rôle ne peut pas créer de rôles (par défaut)
   -S, --no-superuser             le rôle ne sera pas super-utilisateur (par défaut)
   -T, --template=MODÈLE         base de données modèle à copier
   -U, --username=NOMUTILISATEUR nom d'utilisateur pour la connexion
   -U, --username=NOMUTILISATEUR  nom d'utilisateur pour la connexion
   -U, --username=NOMUTILISATEUR  nom de l'utilisateur pour la connexion (pas
                                 celui à créer)
   -U, --username=NOMUTILISATEUR  nom de l'utilisateur pour la connexion (pas
                                 celui à supprimer)
   -U, --username=NOMUTILISATEUR  nom d'utilisateur pour la connexion
   -V, --version                affiche la version puis quitte
   -V, --version              affiche la version puis quitte
   -V, --version              affiche la version puis quitte
   -V, --version              affiche la version puis quitte
   -W, --password                force la demande d'un mot de passe
   -W, --password                force la demande d'un mot de passe
   -Z, --analyze-only            met seulement à jour les statistiques de
                                l'optimiseur
   -a, --all                       exécute VACUUM sur toutes les bases de
                                  données
   -a, --all                 réorganise toutes les bases de données
   -a, --all                réindexe toutes les bases de données
   -c, --connection-limit=N       nombre maximal de connexions pour le rôle
                                 (par défaut sans limite)
   -d, --createdb                 l'utilisateur peut créer des bases de
                                 données
   -d, --dbname=NOMBASE            exécute VACUUM sur cette base de données
   -d, --dbname=NOMBASE           base de données à partir de laquelle
                                 supprimer le langage
   -d, --dbname=NOMBASE      base de données à réorganiser
   -d, --dbname=NOMBASE           base sur laquelle installer le langage
   -d, --dbname=NOMBASE     base de données à réindexer
   -d, --dbname=NOMBASE     base de données
   -e, --echo                      affiche les commandes envoyées au serveur
   -e, --echo                    affiche les commandes envoyées au serveur
   -e, --echo                     affiche les commandes envoyées au serveur
   -f, --full                      exécute VACUUM en mode FULL
   -g, --role=ROLE                le nouveau rôle sera un membre de ce rôle
   -h, --host=HOTE               hôte du serveur de bases de données
                                ou répertoire des sockets
   -h, --host=HOTE                hôte du serveur de bases de données ou
                                 répertoire des sockets
   -h, --host=NOMHÔTE           hôte du serveur de bases de données ou
                               répertoire des sockets
   -i, --index=INDEX        recrée uniquement cet (ces) index
   -i, --inherit                  le rôle hérite des droits des rôles dont il
                                 est membre (par défaut)
   -i, --interactive         demande confirmation avant de supprimer quoi que
                            ce soit
   -i, --interactive         demande confirmation avant de supprimer quoi que
                            ce soit, et demande le nom du rôle s'il n'est pas
                            indiqué
   -l, --list                     affiche la liste des langages déjà
                                 installés
   -l, --locale=LOCALE           paramètre de la locale pour la base de
                                données
   -l, --login                    le rôle peut se connecter (par défaut)
   -p, --port=PORT               port du serveur de bases de données
   -p, --port=PORT                port du serveur de bases de données
   -p, --port=PORT                port du serveur de bases de données
   -q, --quiet                     n'écrit aucun message
   -q, --quiet               n'écrit aucun message
   -q, --quiet               s'exécute sans affichage
   -r, --createrole               le rôle peut créer des rôles
   -s, --superuser                le rôle est super-utilisateur
   -s, --system             réindexe les catalogues système
   -t, --table='TABLE[(COLONNES)]' exécute VACUUM sur cette (ces) tables
   -t, --table=TABLE         réorganise uniquement cette(ces) table(s)
   -t, --table=TABLE        réindexe uniquement cette (ces) table(s)
   -t, --timeout=SECS       durée en secondes à attendre lors d'une tentative de connexion
                           0 pour désactiver (défaut: %s)
   -v, --verbose                   mode verbeux
   -v, --verbose                 mode verbeux
   -w, --no-password             empêche la demande d'un mot de passe
   -w, --no-password             empêche la demande d'un mot de passe
   -z, --analyze                 met à jour les statistiques de l'optimiseur
 %s (%s/%s)  %s nettoie et analyse une base de données PostgreSQL.

 %s réorganise toutes les tables précédemment réorganisées au sein d'une base
de données via la commande CLUSTER.

 %s crée une base de données PostgreSQL.

 %s crée un nouvel rôle PostgreSQL.

 %s installe un langage de procédures dans une base de données PostgreSQL.

 %s produitun test de connexion à une base de données PostgreSQL.

 %s réindexe une base de données PostgreSQL.

 %s supprime une base de données PostgreSQL.

 %s supprime un rôle PostgreSQL.

 %s supprime un langage procédural d'une base de données.

 %s : « %s » n'est pas un nom d'encodage valide
 %s : %s %s : ne réorganise pas à la fois toutes les bases de données et une base
spécifique via la commande CLUSTER
 %s : impossible de réorganiser la(les) table(s) spécifique(s) dans toutes les bases de données
 %s : ne peut pas réindexer toutes les bases de données et une base
spécifique en même temps
 %s : ne peut pas réindexer toutes les bases de données et les catalogues
système en même temps
 %s : ne peut pas réindexer un (des) index spécifique(s) et
les catalogues système en même temps
 %s : ne peut pas réindexer un (des) index spécifique(s) dans toutes les
bases de données
 %s : ne peut pas réindexer une (des) table(s) spécifique(s) etles catalogues système en même temps
 %s : ne peut pas réindexer une (des) table(s) spécifique(s) dans toutes
les bases de données
 %s : ne peut utiliser l'option « freeze » lors de l'exécution d'un ANALYZE
seul
 %s : ne peut utiliser l'option « full » lors de l'exécution d'un ANALYZE seul
 %s : ne peut pas exécuter VACUUM sur toutes les bases de données et sur une
base spécifique en même temps
 %s : ne peut pas exécuter VACUUM sur une(des)  table(s) spécifique(s)
dans toutes les bases de données
 %s : réorganisation de la base de données « %s » via la commande CLUSTER
 %s : la réorganisation de la base de données « %s » via la commande
CLUSTER a échoué : %s %s : la réorganisation de la table « %s » de la base de données « %s » avec
la commande CLUSTER a échoué : %s %s: l'ajout du commentaire a échoué (la base de données a été créée) : %s %s : n'a pas pu se connecter à la base de données %s
 %s : n'a pas pu se connecter à la base de données %s : %s %s : n'a pas pu récupérer les options par défaut
 %s : la création du nouvel rôle a échoué : %s %s : la création de la base de données a échoué : %s %s: la suppression de la base de données a échoué : %s %s : le langage « %s » est déjà installé sur la base de données « %s »
 %s : le langage « %s » n'est pas installé dans la base de données « %s »
 %s : l'installation du langage a échoué : %s %s : la suppression du langage a échoué : %s %s : argument nom de la base de données requis mais manquant
 %s : argument nom du langage requis mais manquant
 %s : argument nom du rôle requis mais manquant
 %s : une seule des options --locale et --lc-collate peut être indiquée
 %s : une seule des options --locale et --lc-ctype peut être indiquée
 %s : échec de la requête : %s %s : la requête était : %s
 %s : réindexation de la base de données « %s »
 %s : la réindexation de la base de données « %s » a échoué : %s %s : la réindexation de l'index « %s » dans la base de données « %s » a
échoué : %s %s : la réindexation des catalogues système a échoué : %s %s : la réindexation de la table « %s » dans la base de données « %s » a
échoué : %s %s : la suppression du rôle « %s » a échoué : %s %s : trop d'arguments en ligne de commande (le premier étant « %s »)
 %s : exécution de VACUUM sur la base de données « %s »
 %s : l'exécution de VACUUM sur la base de données « %s » a échoué : %s %s : l'exécution de VACUUM sur la table « %s » dans la base de données
« %s » a échoué : %s Êtes-vous sûr ? Requête d'annulation envoyée
 N'a pas pu envoyer la requête d'annulation : %s La base de données « %s » sera définitivement supprimée.
 Le saisir de nouveau :  Saisir le nom du rôle à ajouter :  Saisir le nom du rôle à supprimer :  Saisir le mot de passe pour le nouveau rôle :  Génération de statistiques complètes pour l'optimiseur Génération de statistiques moyennes pour l'optimiseur (dix cibles) Génération de statistiques minimales pour l'optimiseur (une cible) Nom Échec du chiffrement du mot de passe.
 Mot de passe :  Les mots de passe ne sont pas identiques.
 Merci de répondre « %s » ou « %s ».
 Langages procéduraux Le rôle « %s » sera définitivement supprimé.
 Le nouveau rôle est-il super-utilisateur ? Le nouveau rôle est-il autorisé à créer des bases de données ? Le nouveau rôle est-il autorisé à créer de nouveaux rôles ? De confiance (trusted) ? Essayer « %s --help » pour plus d'informations.
 Usage :
 acceptation des connexions
 ne peut pas dupliquer un pointeur nul (erreur interne)
 n'a pas pu trouver l'identifiant réel %ld de l'utilisateur : %s n non pas de tentative
 pas de réponse
 mémoire épuisée
 rejet des connexions
 inconnu
 l'utilisateur n'existe pas échec lors de la recherche du nom d'utilisateur : code erreur %lu o oui 