��    �     d  e  �      @&     A&     W&  X   j&  [   �&  B   '  )   b'  "   �'     �'     �'     �'     �'  A   �'  N   =(  P   �(  I   �(  G   ')  N   o)  K   �)  v   
*  F   �*  >   �*  R   +  B   Z+  E   �+  ?   �+  L   #,  3   p,  6   �,  Q   �,  T   --  M   �-  �   �-  ?   �.  9   �.  5   /  :   9/  K   t/  B   �/  D   0  �   H0  3   �0  �   �0  ?   1  D   �1  x   2  6   }2  Q   �2  O   3  F   V3  >   �3  5   �3  >   4  E   Q4  A   �4  O   �4  H   )5  B   r5  @   �5  =   �5  O   46  H   �6  ;   �6  ,   	7  4   67  E   k7  0   �7  =   �7  ,    8  F   M8  D   �8  C   �8  F   9  8   d9  ?   �9  C   �9  =   !:  4   _:  7   �:  L   �:  =   ;  @   W;  6   �;  <   �;  L   <  ,   Y<  (   �<  5   �<  E   �<  T   +=     �=  9   �=  D   �=  G   >     J>     e>  ?   �>  '   �>  +   �>  e   ?  !   z?  ,   �?  4   �?  6   �?  (   5@  /   ^@     �@     �@  $   �@  *   �@  >   A  J   MA  P   �A  N   �A  -   8B     fB     {B     �B  4   �B  $   �B  &   �B  *   !C  #   LC     pC  P   �C  C   �C  A   $D  (   fD     �D     �D     �D     �D  I   �D     HE     hE  
   �E  ,   �E  �   �E  ,   YF     �F  p   �F  e   G  &   rG     �G  `   �G  p   H  4   sH  X   �H  E   I  1   GI  *   yI  -   �I  .   �I  +   J  +   -J  )   YJ  *   �J  >   �J  :   �J  '   (K  =   PK  )   �K     �K  @   �K  7   L  6   LL  6   �L  <   �L  =   �L  4   5M  3   jM  b   �M  9   N  <   ;N  t   xN  #   �N  ,   O  5   >O      tO      �O     �O     �O  '   �O     �O  %   P     'P  /   GP  X   wP     �P  3   �P  (   Q     9Q  )   LQ  #   vQ      �Q     �Q  &   �Q  K   �Q  &   IR     pR  !   �R  (   �R  '   �R     S  #    S     DS  /   dS      �S     �S  #   �S  %   �S     T  ,   8T  $   eT  $   �T  $   �T  6   �T     U      #U  B   DU  �   �U  �   V     �V  $   �V  <   �V  3   /W  '   cW  '   �W  +   �W  (   �W  (   X  #   1X  -   UX  �   �X  +   SY  ,   Y  !   �Y  &   �Y  '   �Y  #   Z     AZ  "   `Z  8   �Z  ,   �Z  $   �Z     [     .[  .   M[  3   |[  &   �[  @   �[  5   \     N\  "   i\  #   �\  ,   �\     �\  !   �\  #   ]  *   B]  #   m]  %   �]  0   �]  &   �]  $   ^     4^     S^     i^  1   �^  "   �^  )   �^  =   _  #   @_  1   d_     �_     �_  :   �_  :   �_  )   ,`  B   V`  P   �`     �`     �`     a     7a     Ga     ea     a  &   �a  &   �a  %   �a  2   b     Cb     Xb  &   tb  !   �b  )   �b  #   �b  "   c     .c  r   ?c  <   �c  D   �c  N   4d     �d      �d     �d  &   �d  )   e  *   .e     Ye  "   se  ,   �e     �e     �e  (   �e  @   !f     bf     �f  P   �f  >   �f  1   ,g  >   ^g  /   �g  Y   �g  >   'h     fh     �h  "   �h     �h  &   �h  <   i     Ci  '   Wi  (   i  '   �i     �i     �i     j  1   j  +   Jj      vj  %   �j  3   �j     �j     
k  ;   #k  "   _k  ;   �k     �k     �k     �k     	l  I   "l  >   ll  D   �l     �l     �l     m  7   .m  9   fm  6   �m  H   �m  @    n     an  3   sn  &   �n  0   �n  6   �n  /   6o  (   fo  ,   �o     �o     �o     �o      p     &p  h   7p  T   �p  �   �p  6   xq  L   �q  J   �q  9   Gr     �r  $   �r  +   �r     �r     �r     s     +s     Cs  /   Ws     �s     �s     �s     �s     �s     t  &   t     <t      Nt     ot  )   �t      �t  !   �t  %   �t  +   u     Bu  &   bu  '   �u     �u     �u  0   �u  .   v  )   Nv  +   xv     �v     �v  4   �v  �   w  #   �w  *   �w     �w     x     x  (   -x  "   Vx     yx  E   �x  #   �x  C   �x     <y     Ty     ly  ;   sy     �y     �y  '   �y  '    z     (z     Hz     `z     |z  B   �z  0   �z  !   {  8   /{  :   h{     �{     �{  3   �{  1   |  +   D|  $   p|  $   �|  h   �|  �  #}  ,         J  �   k  �   �  �   ��  <   �  L   R�     ��     ��  )   ��  &   �  Z   �  s   m�  �   �  �   ��  d   <�  j   ��  �   �  �   ��  p   A�  g   ��  �   �  v   և  y   M�  p   ǈ  �   8�  \   �  h   O�  �   ��  �   P�  ~   �  �   m�  r   R�  T   ō  P   �  S   k�  t   ��  o   4�  v   ��  �   �  [   �  �   k�  ^   9�  g   ��  �    �  `   ͓  {   .�  �   ��  o   ��  d   ��  [   b�  R   ��  K   �  G   ]�  s   ��  U   �  _   o�  e   Ϙ  [   5�  y   ��  �   �  p   ��  R   �  b   p�  o   ӛ  L   C�  _   ��  H   �  r   9�  m   ��  �   �  �   Ğ  f   U�  [   ��  n   �  f   ��  F   �  I   5�  �   �  _   0�  e   ��  _   ��  l   V�  �   ã  P   x�  R   ɤ  D   �  d   a�  �   ƥ     e�  y   h�  ~   �  �   a�  4   �      �  m   >�  P   ��  F   ��  �   D�  A   �  J   "�  �   m�  �   �  Y   ��  [   �  3   <�     p�  H   ��  ^   ج  W   7�  c   ��  i   �  g   ]�  J   Ů  ?   �     P�      f�  h   ��  =   �  3   .�  M   b�  5   ��  ;   �  �   "�  L   ۱  N   (�  G   w�  :   ��  *   ��  0   %�  =   V�  �   ��  >   �  B   V�     ��  V   ��  �   ��  X   �  *   I�  �   t�  �   5�  [   �     L�  �   i�  �   P�  �   �  �   ��     V�  g   ֻ  f   >�  K   ��  a   �  ^   S�  ^   ��  \   �  ]   n�  �   ̾  �   `�  r   �  �   Y�  _   ��  N   B�  �   ��  |   �  u   ��  y   �  �   ��  �   �  q   ��  u   �  �   ��  {   ?�  �   ��  �   K�  J   �  g   ^�  `   ��  D   '�  N   l�     ��     ��  :   ��     #�  F   <�  R   ��  p   ��  �   G�     ��  I   ��  M   G�     ��  V   ��  T   	�  @   ^�  >   ��  A   ��  �    �  =   ��  E   ��  =   &�  I   d�  L   ��  =   ��  ;   9�  ?   u�  j   ��  A    �  G   b�  C   ��  C   ��  0   2�  n   c�  ;   ��  E   �  E   T�  W   ��  3   ��  C   &�  r   j�  =  ��  C  �  .   _�  =   ��  e   ��  P   2�  W   ��  a   ��  C   =�  L   ��  J   ��  K   �  [   e�  �  ��  ^   C�  ^   ��  J   �  Y   L�  Y   ��  D    �  ?   E�  E   ��  ~   ��  V   J�  F   ��  A   ��  ?   *�  ^   j�  b   ��  Z   ,�  w   ��  p   ��  K   p�  ?   ��  C   ��  V   @�  C   ��  S   ��  D   /�  R   t�  O   ��  L   �  ^   d�  ^   ��  5   "�  <   X�  .   ��  2   ��  \   ��  f   T�  g   ��  t   #�  F   ��  T   ��     4�  !   N�  �   p�  e   ��  W   W�  �   ��  g   7�  )   ��  <   ��  :   �     A�  :   [�  8   ��  ?   ��  )   �  )   9�  (   c�  j   ��  M   ��  =   E�  S   ��  @   ��  _   �  !   x�      ��     ��  X  ��  w   2�  j   ��  |   �  =   ��  E   ��  6   �  J   M�  N   ��  Q   ��  -   9�  1   g�  <   ��  '   ��  D   ��  S   C�  r   ��  5   
�  V   @�  �   ��  �   ��  8   �  c   P�  r   ��  �   '�  X   ��  -   �  5   4�  A   j�  /   ��  D   ��  i   !�  5   ��  M   ��  I   �  J   Y�  )   ��  +   ��     ��  l   �  d   ��  D   ��  >   -�  o   l�  .   ��  -   �  n   9�  G   ��  h   ��     Y  2   y  A   �  /   �  b    W   � ]   �    7 U   V +   � �   � �   k �   � �   | �   "    � Z   � ]   3 V   � ]   � P   F N   � @   �     ' >   H +   � D   � ;   � �   4	 �   �	 �  �
 u   u �   � �   z �       � G   � \   � $   M .   r 6   � 5   � "    T   1    � 2   � -   � 1    /   8    h K        � 4   � -   ! R   O P   � K   � R   ? _   � =   � R   0 T   � C   � ;    i   X a   � i   $ a   � 9   � 8   * �   c    G   % :   m B   � -   � 5    6   O /   �    � �   � 5   _ �   � /   * 3   Z    � u   �     6   " A   Y J   � B   � -   ) 0   W 3   � `   � Y    ?   w p   � b   (  5   �  7   �  s   �  Q   m! W   �! D   " U   \" 4  �"    �  �       �   �      7   �           $  �        �       r  n      )  A  ;      =       �     �  �   Y      �   Y   �  �          0   �       C      p              d         S   z  �   b      w   �              d  U  u  -  �   D  ?   �  '  o   �       3   i  	              U   x  �   �               �   p            e   �      ,  [      s      �  ;       �                 >  E  K   �       �   �   g      6               $   �   |      �   �   �  2   �   G           =          �                 �    }   �   j      &   �   �       H   +   �  �   �   n   �   �           (  �  #   �   W   c   �             
  �   �   I       j   �      R      �  G      z   v              m   (           k  �  �   8      �           q          ]         D       �           �  {  l       N   �   v   �   H  �  �   �   �          f  �   �       �      k   5   �     �   2      O       �  y  /       �      i       m  �  s   �   �      g   	   a    �           �       �       "  �   R     �         9  �   �   �   �   �   �               '   3      �   E   �  �  <   �             Q  �   )   �     �       �  �  �  K      �   t  6  P               �  �   Z      �   �            `      %  +  �   X   _  �  �   �  ^    �   W  f   �   .  �     �              �  >   /  �  �     �  �       4            #  }     �   �   �  �   �   �      �       o  B   �   ,   "               J   �     �  �  �    !       V      �                �      �   �         �   �       �  X  �  0  *   \   �     @       �   �   {       �   �   �   \  �   r   �   �  �  L   _       �           l  �   `         �   �  ~  4  �   c      �      O  �   �   �  1   u      S  �      �  9   �         V   @  �      ]       J  %   �   1  �                  *  �  :   �        A   �   �  !  �   N  �   B      T       ^   �      �   a   �         h  Z   Q   �      b   ?  �  �   �   t                .       �   ~   �      �  �       e         &  �   �  �      �           �   �   �   �  [  �   �  q         �       �   �       �      �   �   :         w  <               7      �   �       M  -       |       �  �   P  M       �   �  �  x   �   F      T  �      �              h   �   �     L  
   �   8      �   y       F   I  5  C    
Connection options:
 
General options:
 
If -f/--file is not used, then the SQL script will be written to the standard
output.

 
If no database name is supplied, then the PGDATABASE environment
variable value is used.

 
If no input file name is supplied, then standard input is used.

 
Options controlling the output content:
 
Options controlling the restore:
   %s
   %s [OPTION]...
   %s [OPTION]... [DBNAME]
   %s [OPTION]... [FILE]
   --binary-upgrade             for use by upgrade utilities only
   --column-inserts             dump data as INSERT commands with column names
   --disable-dollar-quoting     disable dollar quoting, use SQL standard quoting
   --disable-triggers           disable triggers during data-only restore
   --exclude-table-data=TABLE   do NOT dump data for the named table(s)
   --inserts                    dump data as INSERT commands, rather than COPY
   --lock-wait-timeout=TIMEOUT  fail after waiting TIMEOUT for a table lock
   --no-data-for-failed-tables  do not restore data of tables that could not be
                               created
   --no-security-labels         do not dump security label assignments
   --no-security-labels         do not restore security labels
   --no-synchronized-snapshots  do not use synchronized snapshots in parallel jobs
   --no-tablespaces             do not dump tablespace assignments
   --no-tablespaces             do not restore tablespace assignments
   --no-unlogged-table-data     do not dump unlogged table data
   --quote-all-identifiers      quote all identifiers, even if not key words
   --role=ROLENAME          do SET ROLE before dump
   --role=ROLENAME          do SET ROLE before restore
   --section=SECTION            dump named section (pre-data, data, or post-data)
   --section=SECTION            restore named section (pre-data, data, or post-data)
   --serializable-deferrable    wait until the dump can run without anomalies
   --use-set-session-authorization
                               use SET SESSION AUTHORIZATION commands instead of
                               ALTER OWNER commands to set ownership
   -1, --single-transaction     restore as a single transaction
   -?, --help                   show this help, then exit
   -?, --help               show this help, then exit
   -C, --create                 create the target database
   -C, --create                 include commands to create database in dump
   -E, --encoding=ENCODING      dump the data in encoding ENCODING
   -F, --format=c|d|t       backup file format (should be automatic)
   -F, --format=c|d|t|p         output file format (custom, directory, tar,
                               plain text (default))
   -I, --index=NAME             restore named index
   -L, --use-list=FILENAME      use table of contents from this file for
                               selecting/ordering output
   -N, --exclude-schema=SCHEMA  do NOT dump the named schema(s)
   -O, --no-owner               skip restoration of object ownership
   -O, --no-owner               skip restoration of object ownership in
                               plain-text format
   -P, --function=NAME(args)    restore named function
   -S, --superuser=NAME         superuser user name to use for disabling triggers
   -S, --superuser=NAME         superuser user name to use in plain-text format
   -S, --superuser=NAME         superuser user name to use in the dump
   -T, --exclude-table=TABLE    do NOT dump the named table(s)
   -T, --trigger=NAME           restore named trigger
   -U, --username=NAME      connect as specified database user
   -V, --version                output version information, then exit
   -V, --version            output version information, then exit
   -W, --password           force password prompt (should happen automatically)
   -Z, --compress=0-9           compression level for compressed formats
   -a, --data-only              dump only the data, not the schema
   -a, --data-only              restore only the data, no schema
   -b, --blobs                  include large objects in dump
   -c, --clean                  clean (drop) database objects before recreating
   -c, --clean                  clean (drop) databases before recreating
   -d, --dbname=CONNSTR     connect using connection string
   -d, --dbname=DBNAME      database to dump
   -d, --dbname=NAME        connect to database name
   -e, --exit-on-error          exit on error, default is to continue
   -f, --file=FILENAME          output file name
   -f, --file=FILENAME          output file or directory name
   -f, --file=FILENAME      output file name
   -g, --globals-only           dump only global objects, no databases
   -h, --host=HOSTNAME      database server host or socket directory
   -j, --jobs=NUM               use this many parallel jobs to dump
   -j, --jobs=NUM               use this many parallel jobs to restore
   -l, --database=DBNAME    alternative default database
   -l, --list               print summarized TOC of the archive
   -n, --schema=NAME            restore only objects in this schema
   -n, --schema=SCHEMA          dump the named schema(s) only
   -o, --oids                   include OIDs in dump
   -p, --port=PORT          database server port number
   -r, --roles-only             dump only roles, no databases or tablespaces
   -s, --schema-only            dump only the schema, no data
   -s, --schema-only            restore only the schema, no data
   -t, --table=NAME             restore named table(s)
   -t, --table=TABLE            dump the named table(s) only
   -t, --tablespaces-only       dump only tablespaces, no databases or roles
   -v, --verbose                verbose mode
   -v, --verbose            verbose mode
   -w, --no-password        never prompt for password
   -x, --no-privileges          do not dump privileges (grant/revoke)
   -x, --no-privileges          skip restoration of access privileges (grant/revoke)
 %s %s dumps a database as a text file or to other formats.

 %s extracts a PostgreSQL database cluster into an SQL script file.

 %s restores a PostgreSQL database from an archive created by pg_dump.

 %s: %s    Command was: %s
 %s: WSAStartup failed: %d
 %s: cannot specify both --single-transaction and multiple jobs
 %s: could not connect to database "%s"
 %s: could not connect to database "%s": %s
 %s: could not connect to databases "postgres" or "template1"
Please specify an alternative database.
 %s: could not get server version
 %s: could not open the output file "%s": %s
 %s: could not parse ACL list (%s) for database "%s"
 %s: could not parse ACL list (%s) for tablespace "%s"
 %s: could not parse server version "%s"
 %s: could not re-open the output file "%s": %s
 %s: dumping database "%s"...
 %s: executing %s
 %s: invalid number of parallel jobs
 %s: maximum number of parallel jobs is %d
 %s: options -d/--dbname and -f/--file cannot be used together
 %s: options -g/--globals-only and -r/--roles-only cannot be used together
 %s: options -g/--globals-only and -t/--tablespaces-only cannot be used together
 %s: options -r/--roles-only and -t/--tablespaces-only cannot be used together
 %s: pg_dump failed on database "%s", exiting
 %s: query failed: %s %s: query was: %s
 %s: running "%s"
 %s: too many command-line arguments (first is "%s")
 %s: unrecognized section name: "%s"
 (The INSERT command cannot set OIDs.)
 (The system catalogs might be corrupted.)
 -C and -1 are incompatible options
 COPY failed for table "%s": %s Consider using a full dump instead of a --data-only dump to avoid this problem.
 Dumping the contents of table "%s" failed: PQgetCopyData() failed.
 Dumping the contents of table "%s" failed: PQgetResult() failed.
 Error from TOC entry %d; %u %u %s %s %s
 Error message from server: %s Error while FINALIZING:
 Error while INITIALIZING:
 Error while PROCESSING TOC:
 NOTICE: there are circular foreign-key constraints among these table(s):
 No matching schemas were found
 No matching tables were found
 Password:  Report bugs to <pgsql-bugs@postgresql.org>.
 Synchronized snapshots are not supported by this server version.
Run with --no-synchronized-snapshots instead if you do not need
synchronized snapshots.
 TOC Entry %s at %s (length %s, checksum %d)
 The command was: %s
 The program "pg_dump" is needed by %s but was not found in the
same directory as "%s".
Check your installation.
 The program "pg_dump" was found by "%s"
but was not the same version as %s.
Check your installation.
 Try "%s --help" for more information.
 Usage:
 WARNING: aggregate function %s could not be dumped correctly for this database version; ignored
 WARNING: archive is compressed, but this installation does not support compression -- no data will be available
 WARNING: archive items not in correct section order
 WARNING: archive was made on a machine with larger integers, some operations might fail
 WARNING: bogus value in pg_cast.castfunc or pg_cast.castmethod field
 WARNING: bogus value in pg_cast.castmethod field
 WARNING: bogus value in proargmodes array
 WARNING: could not find operator with OID %s
 WARNING: could not parse proallargtypes array
 WARNING: could not parse proargmodes array
 WARNING: could not parse proargnames array
 WARNING: could not parse proconfig array
 WARNING: could not parse reloptions array
 WARNING: could not resolve dependency loop among these items:
 WARNING: don't know how to set owner for object type "%s"
 WARNING: errors ignored on restore: %d
 WARNING: ftell mismatch with expected position -- ftell used
 WARNING: invalid creation date in header
 WARNING: line ignored: %s
 WARNING: owner of aggregate function "%s" appears to be invalid
 WARNING: owner of data type "%s" appears to be invalid
 WARNING: owner of function "%s" appears to be invalid
 WARNING: owner of operator "%s" appears to be invalid
 WARNING: owner of operator class "%s" appears to be invalid
 WARNING: owner of operator family "%s" appears to be invalid
 WARNING: owner of schema "%s" appears to be invalid
 WARNING: owner of table "%s" appears to be invalid
 WARNING: requested compression not available in this installation -- archive will be uncompressed
 WARNING: typtype of data type "%s" appears to be invalid
 WARNING: unexpected extra results during COPY of table "%s"
 You might not be able to restore the dump without using --disable-triggers or temporarily dropping the constraints.
 a worker process died unexpectedly
 aborting because of server version mismatch
 actual file length (%s) does not match expected (%s)
 allocating AH for %s, format %d
 already connected to a database
 archiver archiver (db) attempting to ascertain archive format
 bad dumpId
 bad table dumpId for TABLE DATA item
 can only reopen input archives
 cannot duplicate null pointer (internal error)
 cannot restore from compressed archive (compression not supported in this installation)
 compress_io compression is not supported by tar archive format
 compression level must be in range 0..9
 compressor active
 connecting to database "%s" as user "%s"
 connecting to database for restore
 connecting to new database "%s"
 connection needs password
 connection to database "%s" failed: %s corrupt tar header found in %s (expected %d, computed %d) file position %s
 could not change directory to "%s": %s could not close TOC file: %s
 could not close archive file: %s
 could not close compression library: %s
 could not close compression stream: %s
 could not close data file: %s
 could not close directory "%s": %s
 could not close input file: %s
 could not close large object TOC file "%s": %s
 could not close output file: %s
 could not close tar member
 could not close temporary file: %s
 could not commit database transaction could not compress data: %s
 could not create communication channels: %s
 could not create directory "%s": %s
 could not create large object %u: %s could not create worker process: %s
 could not determine seek position in archive file: %s
 could not execute query could not find a "%s" to execute could not find block ID %d in archive -- possibly corrupt archive
 could not find block ID %d in archive -- possibly due to out-of-order restore request, which cannot be handled due to lack of data offsets in archive
 could not find block ID %d in archive -- possibly due to out-of-order restore request, which cannot be handled due to non-seekable input file
 could not find entry for ID %d
 could not find file "%s" in archive
 could not find function definition for function with OID %u
 could not find header for file "%s" in tar archive
 could not find parent extension for %s
 could not find slot of finished worker
 could not generate temporary file name: %s
 could not get server_version from libpq
 could not identify current directory: %s could not identify dependency loop
 could not initialize compression library: %s
 could not obtain lock on relation "%s"
This usually means that someone requested an ACCESS EXCLUSIVE lock on the table after the pg_dump parent process had gotten the initial ACCESS SHARE lock on the table.
 could not open TOC file "%s" for input: %s
 could not open TOC file "%s" for output: %s
 could not open TOC file "%s": %s
 could not open TOC file for input: %s
 could not open TOC file for output: %s
 could not open input file "%s": %s
 could not open input file: %s
 could not open large object %u: %s could not open large object TOC file "%s" for input: %s
 could not open output file "%s" for writing
 could not open output file "%s": %s
 could not open output file: %s
 could not open temporary file
 could not output padding at end of tar member
 could not parse ACL list (%s) for object "%s" (%s)
 could not parse default ACL list (%s)
 could not parse numeric array "%s": invalid character in number
 could not parse numeric array "%s": too many numbers
 could not read binary "%s" could not read directory "%s": %s
 could not read from input file: %s
 could not read from input file: end of file
 could not read input file: %s
 could not read symbolic link "%s" could not reconnect to database: %s could not set default_tablespace to %s: %s could not set default_with_oids: %s could not set search_path to "%s": %s could not set seek position in archive file: %s
 could not set session user to "%s": %s could not start database transaction could not uncompress data: %s
 could not write byte
 could not write byte: %s
 could not write null block at end of tar archive
 could not write to blobs TOC file
 could not write to custom output routine
 could not write to large object (result: %lu, expected: %lu)
 could not write to output file: %s
 could not write to the communication channel: %s
 creating %s %s
 custom archiver database name contains a newline or carriage return: "%s"
 definition of view "%s" appears to be empty (length zero)
 did not find magic string in file header
 direct database connections are not supported in pre-1.3 archives
 directory "%s" does not appear to be a valid archive ("toc.dat" does not exist)
 directory archiver directory name too long: "%s"
 disabling triggers for %s
 dropping %s %s
 dumping contents of table %s
 enabling triggers for %s
 entering main parallel loop
 entering restore_toc_entries_parallel
 entering restore_toc_entries_postfork
 entering restore_toc_entries_prefork
 entry ID %d out of range -- perhaps a corrupt TOC
 error during backup
 error during file seek: %s
 error processing a parallel work item
 error reading large object %u: %s error reading large object TOC file "%s"
 error returned by PQputCopyData: %s error returned by PQputCopyEnd: %s executing %s %s
 expected %d check constraint on table "%s" but found %d
 expected %d check constraints on table "%s" but found %d
 expected format (%d) differs from format found in file (%d)
 failed sanity check, parent OID %u of table "%s" (OID %u) not found
 failed sanity check, parent table OID %u of pg_rewrite entry OID %u not found
 failed to connect to database
 failed to reconnect to database
 file name too long: "%s"
 file offset in dump file is too large
 finding check constraints for table "%s"
 finding default expressions of table "%s"
 finding extension tables
 finding inheritance relationships
 finding the columns and types of table "%s"
 finished item %d %s %s
 finished main parallel loop
 flagging inherited columns in subtables
 found unexpected block ID (%d) when reading data -- expected %d
 identifying extension members
 implied data-only restore
 incomplete tar header found (%lu byte)
 incomplete tar header found (%lu bytes)
 input file appears to be a text format dump. Please use psql.
 input file does not appear to be a valid archive
 input file does not appear to be a valid archive (too short?)
 input file is too short (read %lu, expected 5)
 internal error -- WriteData cannot be called outside the context of a DataDumper routine
 internal error -- neither th nor fh specified in tarReadRaw()
 invalid ENCODING item: %s
 invalid OID for large object
 invalid OID for large object (%u)
 invalid STDSTRINGS item: %s
 invalid adnum value %d for table "%s"
 invalid argument string (%s) for trigger "%s" on table "%s"
 invalid binary "%s" invalid client encoding "%s" specified
 invalid column number %d for table "%s"
 invalid column numbering in table "%s"
 invalid compression code: %d
 invalid dependency %d
 invalid dumpId %d
 invalid line in large object TOC file "%s": "%s"
 invalid message received from worker: "%s"
 invalid number of parallel jobs
 invalid output format "%s" specified
 large-object output not supported in chosen format
 last built-in OID is %u
 launching item %d %s %s
 mismatch in actual vs. predicted file position (%s vs. %s)
 missing index for constraint "%s"
 moving from position %s to next member at file position %s
 no item ready
 no output directory specified
 not built with zlib support
 now at file position %s
 options --inserts/--column-inserts and -o/--oids cannot be used together
 options -c/--clean and -a/--data-only cannot be used together
 options -s/--schema-only and -a/--data-only cannot be used together
 out of memory
 out of on_exit_nicely slots
 parallel archiver parallel backup only supported by the directory format
 parallel restore from non-seekable file is not supported
 parallel restore from standard input is not supported
 parallel restore is not supported with archives made by pre-8.0 pg_dump
 parallel restore is not supported with this archive file format
 pclose failed: %s pgpipe: could not accept connection: error code %d
 pgpipe: could not bind: error code %d
 pgpipe: could not connect socket: error code %d
 pgpipe: could not create second socket: error code %d
 pgpipe: could not create socket: error code %d
 pgpipe: could not listen: error code %d
 pgpipe: getsockname() failed: error code %d
 processing %s
 processing data for table "%s"
 processing item %d %s %s
 processing missed item %d %s %s
 query failed: %s query produced null referenced table name for foreign key trigger "%s" on table "%s" (OID of table: %u)
 query returned %d row instead of one: %s
 query returned %d rows instead of one: %s
 query to get data of sequence "%s" returned %d row (expected 1)
 query to get data of sequence "%s" returned %d rows (expected 1)
 query to get data of sequence "%s" returned name "%s"
 query to get rule "%s" for table "%s" failed: wrong number of rows returned
 query to obtain definition of view "%s" returned more than one definition
 query to obtain definition of view "%s" returned no data
 query was: %s
 read TOC entry %d (ID %d) for %s %s
 reading column info for interesting tables
 reading constraints
 reading default privileges
 reading dependency data
 reading event triggers
 reading extensions
 reading foreign key constraints for table "%s"
 reading indexes
 reading indexes for table "%s"
 reading large objects
 reading procedural languages
 reading rewrite rules
 reading schemas
 reading table inheritance information
 reading triggers
 reading triggers for table "%s"
 reading type casts
 reading user-defined aggregate functions
 reading user-defined collations
 reading user-defined conversions
 reading user-defined foreign servers
 reading user-defined foreign-data wrappers
 reading user-defined functions
 reading user-defined operator classes
 reading user-defined operator families
 reading user-defined operators
 reading user-defined tables
 reading user-defined text search configurations
 reading user-defined text search dictionaries
 reading user-defined text search parsers
 reading user-defined text search templates
 reading user-defined types
 reducing dependencies for %d
 restored %d large object
 restored %d large objects
 restoring data out of order is not supported in this archive format: "%s" is required, but comes before "%s" in the archive file.
 restoring large object with OID %u
 sanity check on integer size (%lu) failed
 saving database definition
 saving encoding = %s
 saving large objects
 saving standard_conforming_strings = %s
 schema with OID %u does not exist
 select() failed: %s
 server version must be at least 7.3 to use schema selection switches
 server version: %s; %s version: %s
 shell command argument contains a newline or carriage return: "%s"
 skipping item %d %s %s
 skipping tar member %s
 sorter table "%s" could not be created, will not restore its data
 tar archiver this format cannot be read
 transferring dependency %d -> %d to %d
 unexpected COPY statement syntax: "%s"
 unexpected data offset flag %d
 unexpected end of file
 unexpected section code %d
 unexpected tgtype value: %d
 unrecognized archive format "%s"; please specify "c", "d", or "t"
 unrecognized command received from master: "%s"
 unrecognized constraint type: %c
 unrecognized data block type %d while restoring archive
 unrecognized data block type (%d) while searching archive
 unrecognized encoding "%s"
 unrecognized file format "%d"
 unrecognized object type in default privileges: %d
 unrecognized provolatile value for function "%s"
 unsupported version (%d.%d) in file header
 warning from original dump file: %s
 worker process failed: exit code %d
 wrote %lu byte of large object data (result = %lu)
 wrote %lu bytes of large object data (result = %lu)
 Project-Id-Version: pg_dump (PostgreSQL 9)
Report-Msgid-Bugs-To: pgsql-bugs@postgresql.org
POT-Creation-Date: 2017-08-22 15:44+0000
PO-Revision-Date: 2017-08-18 15:32+0300
Last-Translator: Alexander Lakhin <exclusion@gmail.com>
Language-Team: Russian <pgsql-ru-general@postgresql.org>
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 
Параметры подключения:
 
Общие параметры:
 
Если не указан параметр -f/--file, SQL-скрипт записывается в стандартный вывод.

 
Если имя базы данных не указано, используется переменная окружения PGDATABASE.

 
Если имя входного файла не указано, используется стандартное устройство ввода.

 
Параметры, управляющие выводом:
 
Параметры, управляющие восстановлением:
   %s
   %s [ПАРАМЕТР]...
   %s [ПАРАМЕТР]... [ИМЯ_БД]
   %s [ПАРАМЕТР]... [ФАЙЛ]
   --binary-upgrade             только для утилит обновления БД
   --column-inserts             выгружать данные в виде INSERT с именами колонок
   --disable-dollar-quoting     отключить спецстроки с $, выводить строки
                               по стандарту SQL
   --disable-triggers           отключить триггеры при восстановлении
                               только данных, без схемы
   --exclude-table-data=ТАБЛИЦА НЕ выгружать указанную таблицу(ы)
   --inserts                    выгрузить данные в виде команд INSERT, не COPY
   --lock-wait-timeout=ТАЙМАУТ  прервать операцию при таймауте блокировки таблицы
   --no-data-for-failed-tables  не восстанавливать данные таблиц, которые
                               не удалось создать
   --no-security-labels         не выгружать назначения меток безопасности
   --no-security-labels         не восстанавливать метки безопасности
   --no-synchronized-snapshots  не использовать синхронизированные снимки
                               в параллельных заданиях
   --no-tablespaces             не выгружать назначения табличных пространств
   --no-tablespaces             не восстанавливать назначения табл. пространств
   --no-unlogged-table-data     не выгружать данные нежурналируемых таблиц
   --quote-all-identifiers      заключать в кавычки все идентификаторы,
                               а не только ключевые слова
   --role=ИМЯ_РОЛИ          выполнить SET ROLE перед выгрузкой
   --role=ИМЯ_РОЛИ          выполнить SET ROLE перед восстановлением
   --section=РАЗДЕЛ             выгрузить заданный раздел
                               (pre-data, data или post-data)
   --section=РАЗДЕЛ             восстановить заданный раздел
                               (pre-data, data или post-data)
   --serializable-deferrable    дождаться момента для выгрузки данных без аномалий
   --use-set-session-authorization
                               устанавливать владельца, используя команды
                               SET SESSION AUTHORIZATION вместо ALTER OWNER
   -1, --single-transaction     выполнить восстановление в одной транзакции
   -?, --help                   показать эту справку и выйти
   -?, --help               показать эту справку и выйти
   -C, --create                 создать целевую базу данных
   -C, --create                 добавить в копию команды создания базы данных
   -E, --encoding=КОДИРОВКА     выгружать данные в заданной кодировке
   -F, --format=c|d|t       формат файла (должен определяться автоматически)
   -F, --format=c|d|t|p         формат выводимых данных
                               (пользовательский | каталог | tar |
                               текстовый (по умолчанию))
   -I, --index=ИМЯ              восстановить указанный индекс
   -L, --use-list=ИМЯ_ФАЙЛА     использовать оглавление из этого файла для
                               чтения/упорядочивания данных
   -N, --exclude-schema=СХЕМА   НЕ выгружать указанную схему(ы)
   -O, --no-owner               не восстанавливать владение объектами
   -O, --no-owner               не восстанавливать владение объектами
                               при использовании текстового формата
   -P, --function=ИМЯ(арг-ты)   восстановить заданную функцию
   -S, --superuser=ИМЯ          имя суперпользователя для отключения триггеров
   -S, --superuser=ИМЯ          имя пользователя, который будет задействован
                               при восстановлении из текстового формата
   -S, --superuser=ИМЯ          имя пользователя для выполнения выгрузки
   -T, --exclude-table=ТАБЛИЦА  НЕ выгружать указанную таблицу(ы)
   -T, --trigger=ИМЯ            восстановить заданный триггер
   -U, --username=ИМЯ       имя пользователя баз данных
   -V, --version                показать версию и выйти
   -V, --version            показать версию и выйти
   -W, --password           запрашивать пароль всегда (обычно не требуется)
   -Z, --compress=0-9           уровень сжатия при архивации
   -a, --data-only              выгрузить только данные, без схемы
   -a, --data-only              восстановить только данные, без схемы
   -b, --blobs                  выгрузить также большие объекты
   -c, --clean                  очистить (удалить) объекты БД при восстановлении
   -c, --clean                  очистить (удалить) базы данных перед
                               восстановлением
   -d, --dbname=СТРОКА      подключиться с данной строкой подключения
   -d, --dbname=БД          имя базы данных для выгрузки
   -d, --dbname=БД          подключиться к указанной базе данных
   -e, --exit-on-error          выйти при ошибке (по умолчанию - продолжать)
   -f, --file=ИМЯ_ФАЙЛА         имя выходного файла
   -f, --file=ИМЯ               имя выходного файла или каталога
   -f, --file=ИМЯ_ФАЙЛА     имя выходного файла
   -g, --globals-only           выгрузить только глобальные объекты, без баз
   -h, --host=ИМЯ           имя сервера баз данных или каталог сокетов
   -j, --jobs=ЧИСЛО             распараллелить копирование на указанное число
                               заданий
   -j, --jobs=ЧИСЛО             распараллелить восстановление на указанное число заданий
   -l, --database=ИМЯ_БД    выбор другой базы данных по умолчанию
   -l, --list               вывести краткое оглавление архива
   -n, --schema=ИМЯ             восстановить объекты только в этой схеме
   -n, --schema=СХЕМА           выгрузить только указанную схему(ы)
   -o, --oids                   выгружать данные с OID
   -p, --port=ПОРТ          номер порта сервера БД
   -r, --roles-only             выгрузить только роли, без баз данных
                               и табличных пространств
   -s, --schema-only            выгрузить только схему, без данных
   -s, --schema-only            восстановить только схему, без данных
   -t, --table=ИМЯ              восстановить заданную таблицу(ы)
   -t, --table=ТАБЛИЦА          выгрузить только указанную таблицу(ы)
   -t, --tablespaces-only       выгружать только табличные пространства,
                               без баз данных и ролей
   -v, --verbose                режим подробных сообщений
   -v, --verbose            выводить подробные сообщения
   -w, --no-password        не запрашивать пароль
   -x, --no-privileges          не выгружать права (назначение/отзыв)
   -x, --no-privileges          не восстанавливать права доступа
                               (назначение/отзыв)
 %s %s сохраняет резервную копию БД в текстовом файле или другом виде.

 %s экспортирует всё содержимое кластера баз данных PostgreSQL в SQL-скрипт.

 %s восстанавливает базу данных PostgreSQL из архива, созданного командой pg_dump.

 %s: %s    Выполнялась команда: %s
 %s: ошибка WSAStartup: %d
 %s: параметр --single-transaction допускается только с одним заданием
 %s: не удалось подключиться к базе данных: "%s"
 %s: не удалось подключиться к базе "%s": %s
 %s: не удалось подключиться к базе данных "postgres" или "template1"
Укажите другую базу данных.
 %s: не удалось узнать версию сервера
 %s: не удалось открыть выходной файл "%s": %s
 %s: не удалось разобрать список управления доступом (%s) для базы данных "%s"
 %s: не удалось разобрать список управления доступом (%s) для табл. пространства "%s"
 %s: не удалось разобрать строку версии сервера "%s"
 %s: не удалось повторно открыть выходной файл "%s": %s
 %s: выгрузка базы данных "%s"...
 %s: выполняется %s
 %s: неверное число параллельных заданий
 %s: максимальное число параллельных заданий равно %d
 %s: параметры -d/--dbname и -f/--file исключают друг друга
 %s: параметры -g/--globals-only и -r/--roles-only исключают друг друга
 %s: параметры -g/--globals-only и -t/--tablespaces-only исключают друг друга
 %s: параметры -r/--roles-only и -t/--tablespaces-only исключают друг друга
 %s: ошибка pg_dump для базы данных "%s", выход...
 %s: ошибка при выполнении запроса: %s %s: запрос: %s
 %s: выполняется "%s"
 %s: слишком много аргументов командной строки (первый: "%s")
 %s: нераспознанное имя раздела: "%s"
 (В INSERT нельзя определять OID.)
 (Возможно повреждены системные каталоги.)
 Параметры -C и -1 несовместимы
 сбой команды COPY для таблицы "%s": %s Во избежание этой проблемы, вам вероятно стоит выгружать всю базу данных, а не только данные (--data-only).
 Ошибка выгрузки таблицы "%s": сбой в PQendcopy().
 Ошибка выгрузки таблицы "%s": сбой в PQgetResult().
 Ошибка из записи оглавления %d; %u %u %s %s %s
 Сообщение об ошибке с сервера: %s Ошибка при завершении:
 Ошибка при инициализации:
 Ошибка при обработке оглавления:
 ЗАМЕЧАНИЕ: в следующих таблицах зациклены ограничения внешних ключей:
 Соответствующие схемы не найдены
 Соответствующие таблицы не найдены
 Пароль:  Об ошибках сообщайте по адресу <pgsql-bugs@postgresql.org>.
 В этой версии сервера синхронизированные снимки не поддерживаются.
Если они вам не нужны, укажите при запуске ключ
--no-synchronized-snapshots.
 Запись оглавления %s в %s (длина: %s, контр. сумма: %d)
 Выполнялась команда: %s
 Для %s необходима программа "pg_dump", но она не найдена
в каталоге "%s".
Проверьте правильность установки СУБД.
 Программа "s" найдена в "%s",
но её версия отличается от версии %s.
Проверьте правильность установки СУБД.
 Для дополнительной информации попробуйте "%s --help".
 Использование:
 ПРЕДУПРЕЖДЕНИЕ: агрегатная функция %s не может быть правильно выгружена для этой версии базы данных; функция проигнорирована
 ПРЕДУПРЕЖДЕНИЕ: архив сжат, но установленная версия не поддерживает сжатие -- данные недоступны
 ПРЕДУПРЕЖДЕНИЕ: в последовательности элементов архива нарушен порядок разделов
 ПРЕДУПРЕЖДЕНИЕ: архив был сделан на компьютере большей разрядности -- возможен сбой некоторых операций
 ПРЕДУПРЕЖДЕНИЕ: неприемлемое значение в поле pg_cast.castfunc или pg_cast.castmethod
 ПРЕДУПРЕЖДЕНИЕ: неприемлемое значение в поле pg_cast.castmethod
 ПРЕДУПРЕЖДЕНИЕ: неприемлемое значение в массиве proargmodes
 ПРЕДУПРЕЖДЕНИЕ: оператор с OID %s не найден
 ПРЕДУПРЕЖДЕНИЕ: не удалось разобрать массив proallargtypes
 ПРЕДУПРЕЖДЕНИЕ: не удалось разобрать массив proargmodes
 ПРЕДУПРЕЖДЕНИЕ: не удалось разобрать массив proargnames
 ПРЕДУПРЕЖДЕНИЕ: не удалось разобрать массив proconfig
 ПРЕДУПРЕЖДЕНИЕ: не удалось разобрать массив reloptions
 ПРЕДУПРЕЖДЕНИЕ: не удалось разрешить цикл зависимостей для следующих объектов:
 ПРЕДУПРЕЖДЕНИЕ: неизвестно, как назначить владельца для объекта типа "%s"
 ПРЕДУПРЕЖДЕНИЕ: при восстановлении проигнорировано ошибок: %d
 ПРЕДУПРЕЖДЕНИЕ: позиция ftell не соответствует ожидаемой -- используется ftell
 ПРЕДУПРЕЖДЕНИЕ: неверная дата создания в заголовке
 ПРЕДУПРЕЖДЕНИЕ: строка проигнорирована: %s
 ПРЕДУПРЕЖДЕНИЕ: у агрегатной функции "%s" по-видимому неправильный владелец
 ПРЕДУПРЕЖДЕНИЕ: у типа данных "%s" по-видимому неправильный владелец
 ПРЕДУПРЕЖДЕНИЕ: у функции "%s" по-видимому неправильный владелец
 ПРЕДУПРЕЖДЕНИЕ: у оператора "%s" по-видимому неправильный владелец
 ПРЕДУПРЕЖДЕНИЕ: у класса операторов "%s" по-видимому неправильный владелец
 ПРЕДУПРЕЖДЕНИЕ: у семейства операторов "%s" по-видимому неправильный владелец
 ПРЕДУПРЕЖДЕНИЕ: у схемы "%s" по-видимому неправильный владелец
 ПРЕДУПРЕЖДЕНИЕ: у таблицы "%s" по-видимому неправильный владелец
 ПРЕДУПРЕЖДЕНИЕ: установленная версия программы не поддерживает сжатие -- архив не будет сжиматься
 ПРЕДУПРЕЖДЕНИЕ: у типа данных "%s" по-видимому неправильный тип типа
 ПРЕДУПРЕЖДЕНИЕ: неожиданные лишние результаты получены при COPY для таблицы "%s"
 Возможно для восстановления базы вам потребуется использовать --disable-triggers или временно удалить ограничения.
 рабочий процесс неожиданно прекратился
 продолжение работы с другой версией сервера невозможно
 действительная длина файла (%s) не равна ожидаемой (%s)
 выделение структуры AH для %s, формат %d
 подключение к базе данных уже установлено
 архиватор архиватор (БД) попытка выяснить формат архива
 неверный dumpId
 неверный dumpId таблицы в элементе TABLE DATA
 повторно открыть можно только входные файлы
 попытка дублирования нулевого указателя (внутренняя ошибка)
 восстановить данные из сжатого архива нельзя (установленная версия не поддерживает сжатие)
 compress_io формат архива tar не поддерживает сжатие
 уровень сжатия должен быть в диапазоне 0..9
 сжатие активно
 подключение к базе "%s" с именем пользователя "%s"
 подключение к базе данных для восстановления
 подключение к новой базе данных "%s"
 для подключения необходим пароль
 не удалось подключиться к базе "%s": %s tar-заголовок в %s повреждён (ожидалось: %d, получено: %d), позиция в файле: %s
 не удалось перейти в каталог "%s": %s не удалось закрыть файл оглавления: %s
 не удалось закрыть файл архива: %s
 не удалось закрыть библиотеку сжатия: %s
 не удалось закрыть поток сжатых данных: %s
 не удалось закрыть файл данных: %s
 не удалось закрыть каталог "%s": %s
 не удалось закрыть входной файл: %s
 не удалось закрыть файл оглавления больших объектов "%s": %s
 не удалось закрыть выходной файл: %s
 не удалось закрыть компонент tar-архива
 не удалось закрыть временный файл: %s
 не удалось зафиксировать транзакцию не удалось сжать данные: %s
 не удалось создать каналы межпроцессного взаимодействия: %s
 создать каталог "%s" не удалось: %s
 не удалось создать большой объект %u: %s не удалось создать рабочий процесс: %s
 не удалось определить позицию в файле архива: %s
 не удалось выполнить запрос не удалось найти запускаемый файл "%s" не удалось найти в архиве блок с ID %d -- возможно, архив испорчен
 не удалось найти в архиве блок с ID %d -- возможно, по причине не последовательного запроса восстановления, который нельзя обработать из-за отсутствия смещений данных в архиве
 не удалось найти в архиве блок с ID %d -- возможно, по причине не последовательного запроса восстановления, который нельзя обработать с файлом, не допускающим произвольный доступ
 не найдена запись для ID %d
 не удалось найти файл "%s" в архиве
 не удалось найти определение функции для функции с OID %u
 в архиве tar не найден заголовок для файла "%s"
 не удалось найти родительское расширение для %s
 не удалось найти слот законченного рабочего объекта
 не удалось создать временный файл: %s
 не удалось получить версию сервера из libpq
 не удалось определить текущий каталог: %s не удалось определить цикл зависимостей
 не удалось инициализировать библиотеку сжатия: %s
 не удалось получить блокировку отношения "%s".
Обычно это означает, что кто-то запросил блокировку ACCESS EXCLUSIVE для этой таблицы после того, как родительский процесс pg_dump получил для неё начальную блокировку ACCESS SHARE.
 не удалось открыть для чтения файл оглавления "%s": %s
 не удалось открыть для записи файл оглавления "%s": %s
 не удалось открыть файл оглавления "%s": %s
 не удалось открыть для чтения файл оглавления: %s
 не удалось открыть для записи файл оглавления: %s
 не удалось открыть входной файл "%s": %s
 не удалось открыть входной файл: %s
 не удалось открыть большой объект %u: %s не удалось открыть для чтения файл оглавления больших объектов "%s": %s
 не удалось открыть выходной файл "%s" для записи
 не удалось открыть выходной файл "%s": %s
 не удалось открыть выходной файл: %s
 не удалось открыть временный файл
 не удалось записать выравнивание для компонента tar
 не удалось разобрать список прав (%s) для объекта "%s" (%s)
 не удалось разобрать список прав по умолчанию (%s)
 не удалось разобрать числовой массив "%s": неверный символ в числе
 не удалось разобрать числовой массив "%s": слишком много чисел
 не удалось прочитать исполняемый файл "%s" не удалось прочитать каталог "%s": %s
 не удалось прочитать входной файл: %s
 не удалось прочитать входной файл: конец файла
 не удалось прочитать входной файл: %s
 не удалось прочитать символическую ссылку "%s" не удалось переподключиться к базе: %s не удалось задать для default_tablespace значение %s: %s не удалось установить параметр default_with_oids: %s не удалось присвоить search_path значение "%s": %s не удалось задать текущую позицию в файле архива: %s
 не удалось переключить пользователя сессии на "%s": %s не удаётся начать транзакцию не удалось распаковать данные: %s
 не удалось записать байт
 не удалось записать байт: %s
 не удалось записать нулевой блок в конец tar-архива
 не удалось записать в файл оглавления больших объектов
 не удалось вывести данную в пользовательскую процедуру
 не удалось записать большой объект (результат: %lu, ожидалось: %lu)
 не удалось записать в выходной файл: %s
 не удалось записать в канал взаимодействия: %s
 создаётся %s %s
 внешний архиватор имя базы данных содержит символ новой строки или перевода каретки: "%s"
 определение представления "%s" пустое (длина равна нулю)
 в файле заголовка не найдена магическая строка
 прямые подключения к базе данных не поддерживаются в архивах до версии 1.3
 каталог "%s" не похож на архивный (в нём отсутствует "toc.dat")
 каталоговый архиватор слишком длинное имя каталога: "%s"
 отключаются триггеры таблицы %s
 удаляется %s %s
 выгрузка содержимого таблицы %s
 включаются триггеры таблицы %s
 вход в основной параллельный цикл
 вход в restore_toc_entries_parallel
 вход в restore_toc_entries_postfork
 вход в restore_toc_entries_prefork
 ID записи %d вне диапазона - возможно повреждено оглавление
 ошибка в процессе резервного копирования
 ошибка при перемещении в файле: %s
 ошибка выполнения части параллельной работы
 ошибка чтения большого объекта %u: %s ошибка чтения файла оглавления больших объектов "%s"
 ошибка в PQputCopyData: %s ошибка в PQputCopyEnd: %s выполняется %s %s
 ожидалось %d ограничение-проверка для таблицы "%s", но найдено: %d
 ожидалось %d ограничения-проверки для таблицы "%s", но найдено: %d
 ожидалось %d ограничений-проверок для таблицы "%s", но найдено: %d
 ожидаемый формат (%d) отличается от формата, указанного в файле (%d)
 родительская таблица с OID %u для таблицы "%s" (OID %u) не найдена
 по OID %u не удалось найти родительскую таблицу для записи pg_rewrite с OID %u
 ошибка подключения к базе данных
 ошибка переподключения к базе данных
 слишком длинное имя файла: "%s"
 слишком большое смещение в файле вывода
 поиск ограничений-проверок для таблицы "%s"
 поиск выражений по умолчанию для таблицы "%s"
 поиск таблиц расширений
 поиск связей наследования
 поиск колонок и типов таблицы "%s"
 закончен объект %d %s %s
 основной параллельный цикл закончен
 пометка наследованных колонок в подтаблицах
 при чтении данных получен неожиданный ID блока (%d) -- ожидался: %d
 выявление членов расширений
 подразумевается восстановление только данных
 найден неполный tar-заголовок (размер %lu байт)
 найден неполный tar-заголовок (размер %lu байта)
 найден неполный tar-заголовок (размер %lu байтов)
 входной файл похоже имеет текстовый формат. Загрузите его с помощью psql.
 входной файл не похож на архив
 входной файл не похож на архив (возможно, слишком мал?)
 входной файл слишком короткий (прочитано байт: %lu, ожидалось: 5)
 внутренняя ошибка -- WriteData нельзя вызывать вне контекста процедуры DataDumper
 внутренняя ошибка -- в tarReadRaw() не указан ни th, ни fh
 неверный элемент ENCODING: %s
 неверный OID большого объекта
 неверный OID для большого объекта (%u)
 неверный элемент STDSTRINGS: %s
 неверное значение adnum (%d) в таблице "%s"
 неверная строка аргументов (%s) для триггера "%s" таблицы "%s"
 неверный исполняемый файл "%s" указана неверная клиентская кодировка "%s"
 неверный номер колонки %d для таблицы "%s"
 неверная нумерация колонок в таблице "%s"
 неверный код сжатия: %d
 неверная зависимость %d
 неверный dumpId %d
 неверная строка в файле оглавления больших объектов "%s": "%s"
 от рабочего процесса получено ошибочное сообщение: "%s"
 неверное число параллельных заданий
 указан неверный формат вывода: "%s"
 выбранный формат не поддерживает выгрузку больших объектов
 последний системный OID: %u
 объект %d %s %s запускается
 реальная позиция в файле отличается от предсказанной (%s и %s)
 отсутствует индекс для ограничения "%s"
 переход от позиции %s к следующему компоненту в позиции %s
 элемент не готов
 выходной каталог не указан
 программа собрана без поддержки zlib
 текущая позиция в файле %s
 параметры --inserts/--column-inserts и -o/--oids исключают друг друга
 параметры -c/--clean и -a/--data-only исключают друг друга
 параметры -s/--schema-only и -a/--data-only исключают друг друга
 нехватка памяти
 превышен предел обработчиков штатного выхода
 параллельный архиватор параллельное резервное копирование поддерживается только с форматом "каталог"
 параллельное восстановление возможно только с файлом произвольного доступа
 параллельное восстановление из стандартного ввода не поддерживается
 параллельное восстановление возможно только для архивов, созданных pg_dump версии 8.0 и новее
 параллельное восстановление не поддерживается с выбранным форматом архивного файла
 ошибка pclose: %s pgpipe: не удалось принять соединение (код ошибки: %d)
 pgpipe: не удалось привязаться к сокету (код ошибки: %d)
 pgpipe: не удалось подключить сокет (код ошибки: %d)
 pgpipe: не удалось создать второй сокет (код ошибки: %d)
 pgpipe: не удалось создать сокет (код ошибки: %d)
 pgpipe: не удалось начать приём (код ошибки: %d)
 pgpipe: ошибка в getsockname() (код ошибки: %d)
 обрабатывается %s
 обрабатываются данные таблицы "%s"
 обработка объекта %d %s %s
 обработка пропущенного объекта %d %s %s
 ошибка при выполнении запроса: %s запрос не вернул имя целевой таблицы для триггера внешнего ключа "%s" в таблице "%s" (OID целевой таблицы: %u)
 запрос вернул %d строку вместо одной: %s
 запрос вернул %d строки вместо одной: %s
 запрос вернул %d строк вместо одной: %s
 запрос на получение данных последовательности "%s" вернул %d строку (ожидалась 1)
 запрос на получение данных последовательности "%s" вернул %d строки (ожидалась 1)
 запрос на получение данных последовательности "%s" вернул %d строк (ожидалась 1)
 запрос на получение данных последовательности "%s" вернул имя "%s"
 запрос на получение правила "%s" для таблицы "%s" возвратил неверное число строк
 запрос на получения определения представления "%s" возвратил несколько определений
 запрос на получение определения представления "%s" не возвратил данные
 запрос: %s
 прочитана запись оглавления %d (ID %d): %s %s
 чтение информации о колонках интересующих таблиц
 чтение ограничений
 чтение прав по умолчанию
 чтение данных о зависимостях
 чтение событийных триггеров
 чтение расширений
 чтение ограничений внешних ключей таблицы "%s"
 чтение индексов
 чтение индексов таблицы "%s"
 чтение больших объектов
 чтение процедурных языков
 чтение правил перезаписи
 чтение схем
 чтение информации о наследовании таблиц
 чтение триггеров
 чтение триггеров таблицы "%s"
 чтение приведений типов
 чтение пользовательских агрегатных функций
 чтение пользовательских правил сортировки
 чтение пользовательских преобразований
 чтение пользовательских сторонних серверов
 чтение пользовательских оболочек сторонних данных
 чтение пользовательских функций
 чтение пользовательских классов операторов
 чтение пользовательских семейств операторов
 чтение пользовательских операторов
 чтение пользовательских таблиц
 чтение пользовательских конфигураций текстового поиска
 чтение пользовательских словарей текстового поиска
 чтение пользовательских анализаторов текстового поиска
 чтение пользовательских шаблонов текстового поиска
 чтение пользовательских типов
 уменьшение зависимостей для %d
 восстановлен %d большой объект
 восстановлено %d больших объекта
 восстановлено %d больших объектов
 непоследовательное восстановление данных для данного формата архива не поддерживается: требуется компонент "%s", но в файле архива прежде идёт "%s".
 восстановление большого объекта с OID %u
 несоответствие размера integer (%lu)
 сохранение определения базы данных
 сохранение кодировки (%s)
 сохранение больших объектов
 сохранение standard_conforming_strings (%s)
 схема с OID %u не существует
 ошибка в select(): %s
 для использования параметров выбора схемы нужен сервер версии 7.3 или новее
 версия сервера: %s; версия %s: %s
 аргумент команды оболочки содержит символ новой строки или перевода каретки: "%s"
 объект %d %s %s пропускается
 пропускается компонент tar %s
 sorter создать таблицу "%s" не удалось, её данные не будут восстановлены
 архиватор tar этот формат нельзя прочитать
 переключение зависимости %d -> %d на %d
 недопустимый синтаксис оператора COPY: "%s"
 неожиданный флаг смещения данных: %d
 неожиданный конец файла
 неожиданный код раздела %d
 неожиданное значение tgtype: %d
 нераспознанный формат архива "%s"; укажите "c", "d" или "t"
 от ведущего получена нераспознанная команда: "%s"
 нераспознанный тип ограничения: %c
 нераспознанный тип блока данных %d при восстановлении архива
 нераспознанный тип блока данных (%d) при поиске архива
 нераспознанная кодировка "%s"
 неопознанный формат файла: "%d"
 нераспознанный тип объекта в определении прав по умолчанию: %d)
 недопустимое значение provolatile для функции "%s"
 неподдерживаемая версия (%d.%d) в заголовке файла
 предупреждение из исходного файла: %s
 рабочий процесс завершился с кодом возврата %d
 записан %lu байт данных большого объекта (результат = %lu)
 записано %lu байта данных большого объекта (результат = %lu)
 записано %lu байт данных большого объекта (результат = %lu)
 