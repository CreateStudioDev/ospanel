��    �      T    �      �     �     �     �  !     
   &  -   1  X   _  T   �  `     D   n     �  3   �  K   �  <   E  j   �  >   �  N   ,  @   {  3   �  Y   �  >   J  1   �  j   �  <   &  ?   c  M   �  k   �  J   ]  Y   �  +     0   .  B   _  B   �  *   �  8     �   I  5   �  �     r   �  o   	  1   y  3   �  K   �  -   +  F   Y  \   �  P   �  0   N  4     8   �  4   �  1   "  .   T  @   �  1   �  E   �  B   <          �  (   �  '   �  &   
   ;   1   (   m   -   �   '   �   "   �       !  $   0!  2   U!  ,   �!  +   �!  .   �!  (   "  #   9"  5   ]"  f   �"  ,   �"  9   '#  4   a#  7   �#  =   �#  d   $  "   q$  &   �$  &   �$  #   �$  /   %  >   6%  a   u%  &   �%  &   �%  %   %&  2   K&  1   ~&  0   �&  8   �&  (   '  2   C'      v'  &   �'  '   �'  .   �'  +   (  #   A(  A   e(  2   �(  &   �(  &   )  /   ()  +   X)  4   �)  /   �)  !   �)  1   *  )   =*  (   g*  2   �*  2   �*  3   �*  0   *+  %   [+  #   �+  +   �+     �+     �+  2    ,  #   3,     W,  ,   t,  ,   �,  #   �,  i   �,  i   \-  ?   �-  9   .  "   @.  A   c.  #   �.      �.  9   �.     $/  !   B/  D   d/  &   �/  #   �/  A   �/  -   60  D   d0  !   �0     �0     �0  "   �0      1  9   >1  1   x1  D   �1  ,   �1     2  '   /2  D   W2  8   �2  6   �2     3  E   %3  G   k3  z   �3  c   .4  %   �4  .   �4  2   �4  6   5  #   Q5     u5  %   �5  0   �5  R   �5  ,   86  4   e6  K   �6  @   �6  >   '7  -   f7  ,   �7  '   �7  ,   �7  o   8  k   �8  4   �8  %   '9  (   M9  ;   v9  
   �9  &   �9     �9  /   �9     :  /   +:  �  [:     9<     Q<     k<  "   <  
   �<  =   �<  Q   �<  N   ==  ]   �=  N   �=     9>  5   L>  T   �>  E   �>  f   ?  B   �?  N   �?  F   @  =   ]@  Y   �@  ;   �@  7   1A  v   iA  L   �A  D   -B  q   rB  n   �B  P   SC  k   �C  0   D  4   AD  C   vD  I   �D  8   E  D   =E  �   �E  =   F  �   UF  �   �F  �   �G  :   H  6   RH  l   �H  /   �H  E   &I  [   lI  O   �I  3   J  >   LJ  B   �J  E   �J  ?   K  *   TK  U   K  >   �K  H   L  E   ]L     �L  0   �L  4   �L  2   $M  0   WM  ?   �M  ,   �M  1   �M  .   'N  '   VN  "   ~N  &   �N  ?   �N  5   O  3   >O  2   rO  /   �O  (   �O  C   �O  k   BP  2   �P  <   �P  A   Q  K   `Q  S   �Q  n    R  $   oR  +   �R  >   �R  A   �R  N   AS  `   �S  i   �S  (   [T  .   �T  .   �T  C   �T  @   &U  R   gU  I   �U  ;   V  K   @V  %   �V  -   �V  *   �V  /   W  X   ;W  7   �W  N   �W  9   X  1   UX  /   �X  1   �X  <   �X  ?   &Y  8   fY  C   �Y  \   �Y  /   @Z  -   pZ  7   �Z  :   �Z  A   [  2   S[  (   �[  )   �[  -   �[      \     (\  :   9\  ,   t\      �\  -   �\  /   �\  )    ]  u   J]  u   �]  I   6^  S   �^  '   �^  P   �^  ,   M_  (   z_  H   �_  $   �_  )   `  [   ;`  F   �`  &   �`  M   a  8   Sa  V   �a  &   �a     
b     (b  2   Bb  )   ub  M   �b  7   �b  X   %c  <   ~c     �c  0   �c  S   d  A   Vd  G   �d     �d  Z   �d  ]   Ue  �   �e  �   9f  /   �f  5   �f  8   "g  8   [g  4   �g     �g  %   �g  5   h  a   =h  :   �h  >   �h  k   i  \   �i  P   �i  ;   3j  2   oj  >   �j  2   �j  l   k  o   �k  8   �k  (   *l  3   Sl  E   �l  
   �l  -   �l  
   m  :   m     Lm  ;   ^m     	      �   >   j   n   �   �       �           �      d   c       �   �   |      �   �   X   7      �   2       �   s   A      &       �   4       #          x   �             g   ~   �       �   /   �   \       �   8       �   �   H   _   N   �   �           %   �       o       �   �      i   �   Y   $       [       �   �   3       �   )       �   :       �   q           �   t   w           �   �   5   M   �       0   �   ,   (   �                  
   �       ]   k           �   `   p   E      {       e   "   �   �   z      �          B   �      �   W   S   �           �   �   Z                       l   F   �   U   <   �   K   �      G   ;       �   �   �   L          �   Q   �      b   @       v       C       �   �      6       I       m          T       �          �   .      �   +   }      �   9       O   u   V   �       �           D   �   a   �   �           h                  �                 �              -              1   r   �       ^   �   !      R   �       �   f       '   y   �   ?   *   =   J              P    
Action to be performed:
 
Connection options:
 
General options:
 
Options controlling the output:
 
Options:
 
Report bugs to <pgsql-bugs@postgresql.org>.
       --create-slot      create a new replication slot (for the slot's name see --slot)
       --drop-slot        drop the replication slot (for the slot's name see --slot)
       --start            start streaming in a replication slot (for the slot's name see --slot)
       --xlogdir=XLOGDIR  location for the transaction log directory
   %s [OPTION]...
   -?, --help             show this help, then exit
   -D, --directory=DIR    receive transaction log files into this directory
   -D, --pgdata=DIRECTORY receive base backup into directory
   -F  --fsync-interval=SECS
                         time between fsyncs to the output file (default: %d)
   -F, --format=p|t       output format (plain (default), tar)
   -I, --startpos=LSN     where in an existing slot should the streaming start
   -P, --plugin=PLUGIN    use output plugin PLUGIN (default: %s)
   -P, --progress         show progress information
   -R, --write-recovery-conf
                         write recovery.conf for replication
   -S, --slot=SLOTNAME    name of the logical replication slot
   -S, --slot=SLOTNAME    replication slot to use
   -T, --tablespace-mapping=OLDDIR=NEWDIR
                         relocate tablespace in OLDDIR to NEWDIR
   -U, --username=NAME    connect as specified database user
   -V, --version          output version information, then exit
   -W, --password         force password prompt (should happen automatically)
   -X, --xlog-method=fetch|stream
                         include required WAL files with specified method
   -Z, --compress=0-9     compress tar output with given compression level
   -c, --checkpoint=fast|spread
                         set fast or spread checkpointing
   -d, --dbname=CONNSTR   connection string
   -d, --dbname=DBNAME    database to connect to
   -f, --file=FILE        receive log into this file, - for stdout
   -h, --host=HOSTNAME    database server host or socket directory
   -l, --label=LABEL      set backup label
   -n, --no-loop          do not loop on connection lost
   -o, --option=NAME[=VALUE]
                         pass option NAME with optional value VALUE to the
                         output plugin
   -p, --port=PORT        database server port number
   -r, --max-rate=RATE    maximum transfer rate to transfer data directory
                         (in kB/s, or use suffix "k" or "M")
   -s, --status-interval=INTERVAL
                         time between status packets sent to server (in seconds)
   -s, --status-interval=SECS
                         time between status packets sent to server (default: %d)
   -v, --verbose          output verbose messages
   -w, --no-password      never prompt for password
   -x, --xlog             include required WAL files in backup (fetch mode)
   -z, --gzip             compress tar output
 %*s/%s kB (%d%%), %d/%d tablespace %*s/%s kB (%d%%), %d/%d tablespaces %*s/%s kB (%d%%), %d/%d tablespace (%s%-*.*s) %*s/%s kB (%d%%), %d/%d tablespaces (%s%-*.*s) %*s/%s kB (100%%), %d/%d tablespace %*s %*s/%s kB (100%%), %d/%d tablespaces %*s %s receives PostgreSQL logical change streams.

 %s receives PostgreSQL streaming transaction logs.

 %s takes a base backup of a running PostgreSQL server.

 %s: COPY stream ended before last file was finished
 %s: WAL streaming can only be used in plain mode
 %s: at least one action needs to be specified
 %s: can only write single tablespace to stdout, database has %d
 %s: cannot specify both --xlog and --xlog-method
 %s: cannot use --create-slot or --drop-slot together with --startpos
 %s: cannot use --create-slot or --start together with --drop-slot
 %s: checkpoint completed
 %s: child %d died, expected %d
 %s: child process did not exit normally
 %s: child process exited with error %d
 %s: child thread exited with error %u
 %s: confirming write up to %X/%X, flush to %X/%X (slot %s)
 %s: could not access directory "%s": %s
 %s: could not close compressed file "%s": %s
 %s: could not close directory "%s": %s
 %s: could not close file "%s": %s
 %s: could not connect to server
 %s: could not connect to server: %s
 %s: could not create archive status file "%s": %s
 %s: could not create background process: %s
 %s: could not create background thread: %s
 %s: could not create compressed file "%s": %s
 %s: could not create directory "%s": %s
 %s: could not create file "%s": %s
 %s: could not create pipe for background process: %s
 %s: could not create replication slot "%s": got %d rows and %d fields, expected %d rows and %d fields
 %s: could not create symbolic link "%s": %s
 %s: could not create symbolic link from "%s" to "%s": %s
 %s: could not create timeline history file "%s": %s
 %s: could not determine seek position in file "%s": %s
 %s: could not determine server setting for integer_datetimes
 %s: could not drop replication slot "%s": got %d rows and %d fields, expected %d rows and %d fields
 %s: could not fsync file "%s": %s
 %s: could not fsync log file "%s": %s
 %s: could not get COPY data stream: %s %s: could not get backup header: %s %s: could not get child thread exit status: %s
 %s: could not get transaction log end position from server: %s %s: could not identify system: got %d rows and %d fields, expected %d rows and %d or more fields
 %s: could not initiate base backup: %s %s: could not open directory "%s": %s
 %s: could not open log file "%s": %s
 %s: could not open timeline history file "%s": %s
 %s: could not open transaction log file "%s": %s
 %s: could not pad transaction log file "%s": %s
 %s: could not parse next timeline's starting point "%s"
 %s: could not parse start position "%s"
 %s: could not parse transaction log location "%s"
 %s: could not read COPY data: %s %s: could not read directory "%s": %s
 %s: could not read from ready pipe: %s
 %s: could not receive data from WAL stream: %s %s: could not rename file "%s" to "%s": %s
 %s: could not rename file "%s": %s
 %s: could not seek to beginning of transaction log file "%s": %s
 %s: could not send command to background pipe: %s
 %s: could not send copy-end packet: %s %s: could not send feedback packet: %s %s: could not send replication command "%s": %s %s: could not set compression level %d: %s
 %s: could not set permissions on directory "%s": %s
 %s: could not set permissions on file "%s": %s
 %s: could not stat file "%s": %s
 %s: could not stat transaction log file "%s": %s
 %s: could not wait for child process: %s
 %s: could not wait for child thread: %s
 %s: could not write %u bytes to WAL file "%s": %s
 %s: could not write %u bytes to log file "%s": %s
 %s: could not write timeline history file "%s": %s
 %s: could not write to compressed file "%s": %s
 %s: could not write to file "%s": %s
 %s: creating replication slot "%s"
 %s: directory "%s" exists but is not empty
 %s: directory name too long
 %s: disconnected
 %s: disconnected; waiting %d seconds to try again
 %s: dropping replication slot "%s"
 %s: final receive failed: %s %s: finished segment at %X/%X (timeline %u)
 %s: got WAL data offset %08x, expected %08x
 %s: incompatible server version %s
 %s: incompatible server version %s; client does not support streaming from server versions newer than %s
 %s: incompatible server version %s; client does not support streaming from server versions older than %s
 %s: initiating base backup, waiting for checkpoint to complete
 %s: integer_datetimes compile flag does not match server
 %s: invalid --max-rate unit: "%s"
 %s: invalid checkpoint argument "%s", must be "fast" or "spread"
 %s: invalid compression level "%s"
 %s: invalid fsync interval "%s"
 %s: invalid output format "%s", must be "plain" or "tar"
 %s: invalid port number "%s"
 %s: invalid status interval "%s"
 %s: invalid tablespace mapping format "%s", must be "OLDDIR=NEWDIR"
 %s: invalid tar block header size: %d
 %s: invalid transfer rate "%s": %s
 %s: invalid xlog-method option "%s", must be "fetch" or "stream"
 %s: multiple "=" signs in tablespace mapping
 %s: new directory is not an absolute path in tablespace mapping: %s
 %s: no data returned from server
 %s: no database specified
 %s: no slot specified
 %s: no target directory specified
 %s: no target file specified
 %s: no transaction log end position returned from server
 %s: not renaming "%s%s", segment is not complete
 %s: old directory is not an absolute path in tablespace mapping: %s
 %s: only tar mode backups can be compressed
 %s: out of memory
 %s: received interrupt signal, exiting
 %s: received transaction log record for offset %u with no file open
 %s: replication stream was terminated before stop point
 %s: segment file "%s" has incorrect size %d, skipping
 %s: select() failed: %s
 %s: server reported unexpected history file name for timeline %u: %s
 %s: server reported unexpected next timeline %u, following timeline %u
 %s: server returned unexpected response to BASE_BACKUP command; got %d rows and %d fields, expected %d rows and %d fields
 %s: server stopped streaming timeline %u at %X/%X, but reported next timeline %u to begin at %X/%X
 %s: starting background WAL receiver
 %s: starting log streaming at %X/%X (slot %s)
 %s: starting log streaming at %X/%X (timeline %u)
 %s: starting timeline %u is not present in the server
 %s: streaming header too small: %d
 %s: streaming initiated
 %s: switched to timeline %u at %X/%X
 %s: symlinks are not supported on this platform
 %s: system identifier does not match between base backup and streaming connection
 %s: this build does not support compression
 %s: too many command-line arguments (first is "%s")
 %s: transaction log directory location can only be specified in plain mode
 %s: transaction log directory location must be an absolute path
 %s: transaction log file "%s" has %d bytes, should be 0 or %d
 %s: transfer rate "%s" exceeds integer range
 %s: transfer rate "%s" is not a valid value
 %s: transfer rate "%s" is out of range
 %s: transfer rate must be greater than zero
 %s: unexpected response to TIMELINE_HISTORY command: got %d rows and %d fields, expected %d rows and %d fields
 %s: unexpected result set after end-of-timeline: got %d rows and %d fields, expected %d rows and %d fields
 %s: unexpected termination of replication stream: %s %s: unrecognized link indicator "%c"
 %s: unrecognized streaming header: "%c"
 %s: waiting for background process to finish streaming ...
 Password:  Try "%s --help" for more information.
 Usage:
 cannot duplicate null pointer (internal error)
 out of memory
 transaction log start point: %s on timeline %u
 Project-Id-Version: pg_basebackup (PostgreSQL) 9.4
Report-Msgid-Bugs-To: pgsql-bugs@postgresql.org
POT-Creation-Date: 2017-05-22 16:42+0000
PO-Revision-Date: 2017-05-23 01:51+0100
Last-Translator: Daniele Varrazzo <daniele.varrazzo@gmail.com>
Language-Team: Gruppo traduzioni ITPUG <traduzioni@itpug.org>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Poedit 1.8.7.1
 
Azioni da effettuare:
 
Opzioni di connessione:
 
Opzioni generali:
 
Opzioni di controllo del'output:
 
Opzioni:
 
Puoi segnalare eventuali bug a <pgsql-bugs@postgresql.org>.
       --create-slot      crea un nuovo slot di replica (per il nome vedi --slot)
       --drop-slot        elimina lo slot di replica (per il nome vedi --slot)
       --start            avvia lo streaming in uno slot di replica (per il nome vedi --slot)
       --xlogdir=XLOGDIR  posizione per la directory del log delle transazioni
   %s [OPZIONE]...
   -?, --help             mostra questo aiuto ed esci
   -D, --directory=DIR    ricevi i file di log delle transazioni in questa directory
   -D, --pgdata=DIRECTORY directory in cui ricevere il backup di base
   -F  --fsync-interval=SEC
                         tempo tra i sync del file di output (default: %d)
   -F, --format=p|t       formato di output (plain (default), tar)
   -I, --startpos=LSN     dove deve partire lo streaming in uno slot esistente
   -P, --plugin=PLUGIN    usa il plugin di output PLUGIN (default: %s)
   -P, --progress         mostra informazioni sull'esecuzione
   -R, --write-recovery-conf
                         scrivi recovery.conf per la replica
   -S, --slot=NOMESLOT    nome dello slot di replica logica
   -S, --slot=NOMESLOT    slot di replicazione da usare
   -T, --tablespace-mapping=VECCHIADIR=NUOVADIR
                         sposta il tablespace da VECCHIADIR a NUOVADIR
   -U, --username=NAME    connettiti al database col nome utente specificato
   -V, --version          mostra informazioni sulla versione ed esci
   -W, --password         forza la richiesta della password
                         (dovrebbe essere automatico)
   -X, --xlog-method=fetch|stream
                         includi i file WAL richiesti col metodo specificato
   -Z, --compress=0-9     comprimi l'output tar a questo livello di compressione
   -c, --checkpoint=fast|spread
                         imposta punti di controllo più veloci o più radi
   -d, --dbname=CONNSTR   stringa di connessione
   -d, --dbname=NOMEDB    database a cui connettersi
   -f, --file=FILE        riceve i log in questo file, - per stdout
   -h, --host=HOSTNAME    host del server database o directory del socket
   -l, --label=LABEL      imposta l'etichetta del backup
   -n, --no-loop          non ri-eseguire se la connessione è persa
   -o, --option=NOME[=VALORE]
                         passa l'opzione NOME col valore opzionale VALORE
                         al plugin di output
   -p, --port=PORT        numero di porta del server database
   -r, --max-rate=RATE    transfer rate massimo per trasferire la directory dei dati
                         (in kB/s, oppure usa i suffissi "k" o "M")
   -s, --status-interval=INTERVAL
                         intervallo tra i pacchetti di stato inviati al server
                         (in secondi)
   -s, --status-interval=SEC
                         tempo tra gli invii dei pacchetti di stato al server
                         (default: %d)
   -v, --verbose          messaggi di output più numerosi
   -w, --no-password      non chiedere mai la password
   -x, --xlog             includi i file WAL necessari nel backup
                         (modalità fetch)
   -z, --gzip             comprimi l'output tar
 %*s/%s kB (%d%%), %d/%d tablespace %*s/%s kB (%d%%), %d/%d tablespace %*s/%s kB (%d%%), %d/%d tablespace (%s%-*.*s) %*s/%s kB (%d%%), %d/%d tablespace (%s%-*.*s) %*s/%s kB (100%%), %d/%d tablespace %*s %*s/%s kB (100%%), %d/%d tablespace %*s %s riceve stream di modifiche logiche PostgreSQL.

 %s riceve lo stream del log delle transazioni di PostgreSQL.

 %s crea un backup di base di un server PostgreSQL in esecuzione.

 %s: lo stream COPY è terminato prima che l'ultimo file fosse finito
 %s: lo streaming WAL può essere usato solo in modalità plain
 %s: occorre specificare almeno una azione
 %s: è possibile scrivere solo un singolo tablespace su stdout, il database ne ha %d
 %s: non è possibile specificare sia --xlog che --xlog-method
 %s: --create-slot o --drop-slot non possono essere usate con --startpos
 %s: --create-slot o --start non possono essere usate con --drop-slot
 %s: checkpoint completato
 %s: il processo figlio %d interrotto, atteso %d
 %s: il processo figlio non è terminato normalmente
 %s: il processo figlio è terminato con errore %d
 %s: il thread figlio è terminato con errore %u
 %s: scritture confermate fino a %X/%X, flush a %X/%X (slot %s)
 %s: accesso alla directory "%s" fallito: %s
 %s: chiusura del file compresso "%s" fallita: %s
 %s: chiusura della directory "%s" fallita: %s
 %s: chiusura del file "%s" fallita: %s
 %s: connessione al server fallita
 %s: connessione al server fallita: %s
 %s: creazione del file di stato dell'archivio "%s" fallita: %s
 %s: creazione del processo in background fallita: %s
 %s: creazione del thread in background fallita: %s
 %s: creazione del file compresso "%s" fallita: %s
 %s: creazione della directory "%s" fallita: %s
 %s: creazione del file "%s" fallita: %s
 %s: creazione della pipe per il processo in background fallita: %s
 %s: creazione dello slot di replica "%s" fallita: ricevute %d righe e %d campi, attesi %d righe e %d campi
 %s: creazione del link simbolico "%s" fallita: %s
 %s: creazione del link simbolico da "%s" a "%s" fallita: %s
 %s: creazione del file di storia della timeline "%s" fallita: %s
 %s: determinazione della posizione dove muoversi nel file "%s" fallita: %s
 %s: non è stato possibile determinare l'impostazione integer_datetimes del server
 %s: eliminazione dello slot di replica "%s" fallita: ricevute %d righe e %d campi, attesi %d righe e %d campi
 %s: fsync del file "%s" fallito: %s
 %s: fsync del file di log "%s" fallito: %s
 %s: non è stato possibile ottenere lo stream di dati COPY: %s %s: non è stato possibile ottenere l'intestazione del backup: %s %s: non è stato possibile ottenere il codice di uscita del thread figlio: %s
 %s: non è stato possibile ottenere la posizione finale del log delle transazioni dal server: %s %s: identificazione del sistema fallita: ricevute %d righe e %d campi, attese %d righe e %d campi o più
 %s: avvio del backup di base fallito: %s %s: apertura della directory "%s" fallita: %s
 %s: apertura del file di log "%s" fallita: %s
 %s: apertura del file della storia della timeline "%s" fallita: %s
 %s: apertura del file di log delle transazioni "%s" fallita: %s
 %s: correzione della lunghezza del file di log delle transazioni "%s" fallita: %s
 %s: interpretazione del punto d'inizio della nuova timeline "%s" fallita
 %s: interpretazione della posizione di inizio "%s" fallita
 %s: interpretazione della posizione del log delle transazioni "%s" fallita
 %s: lettura dei dati COPY fallita: %s %s: lettura della directory "%s" fallita: %s
 %s: lettura dalla pipe pronta fallita: %s
 %s: ricezione dati dallo stream WAL fallita: %s %s: non è stato possibile rinominare il file di storia della timeline "%s" in "%s": %s
 %s: non è stato possibile rinominare il file "%s": %s
 %s: spostamento all'inizio del file di log delle transazioni "%s" fallito: %s
 %s invio del comando alla pipe di background fallita: %s
 %s: invio del pacchetto di fine copia fallito: %s %s: invio del pacchetto di feedback fallito: %s %s: invio del comando di replica "%s" fallito: %s %s: impostazione del livello di compressione %d fallito: %s
 %s: impostazione dei permessi sulla directory "%s" fallita: %s
 %s: impostazione dei permessi sul file "%s" fallita: %s
 %s: non è stato possibile ottenere informazioni sul file "%s": %s
 %s: non è stato possibile ottenere informazioni sul file di log delle transazioni "%s": %s
 %s: errore nell'attesa del processo figlio: %s
 %s: errore nell'attesa del thread figlio: %s
 %s: scrittura di %u byte nel file WAL "%s" fallita: %s
 %s: scrittura di %u byte nel file di log "%s" fallita: %s
 %s: scrittura del file di storia della timeline "%s" fallita: %s
 %s: scrittura nel file compresso "%s" fallita: %s
 %s: scrittura nel file "%s" fallita: %s
 %s: creazione dello slot di replica "%s"
 %s: la directory "%s" esiste ma non è vuota
 %s: nome directory troppo lungo
 %s: disconnesso
 %s: disconnesso; aspetterò %d secondi prima di riprovare
 %s: eliminazione dello slot di replica "%s"
 %s: ricezione finale fallita: %s %s: terminato segmento a %X/%X (timeline %u)
 %s: ricevuto offset dati WAL %08x, atteso %08x
 %s: versione del server incompatibile %s
 %s: server di versione %s non compatibile; il client non supporta lo streaming da server di versione successiva a %s
 %s: server di versione %s non compatibile; il client non supporta lo streaming da server di versione precedente a %s
 %s: avvio del backup di base, in attesa del completamento del checkpoint
 %s: l'opzione di compilazione integer_datetimes non combacia con quella del server
 %s: unità --max-rate non valida: "%s"
 %s: argomento di checkpoint "%s" non valido, deve essere "fast" oppure "spread"
 %s: livello di compressione non valido "%s"
 %s: intervallo di fsync "%s" non valido
 %s: formato di output "%s" non valido, deve essere "plain" oppure "tar"
 %s: numero di porta non valido "%s"
 %s: intervallo di status "%s" non valido
 %s: formato di mappatura dei tablespace "%s" non valido, deve essere "VECCHIADIR=NUOVADIR"
 %s: dimensione del blocco di intestazione del file tar non valida: %d
 %s: transfer rate non valido "%s": %s
 %s: opzione xlog-method "%s" non valida, deve essere "fetch" oppure "stream"
 %s: più di un segno "=" nella mappatura dei tablespace
 %s: la nuova directory non è un percorso assoluto nella mappatura dei tablespace: %s
 %s: nessun dato restituito dal server
 %s: database non specificato
 %s: slot non specificato
 %s: nessuna directory di destinazione specificata
 %s: file di destinazione non specificato
 %s: nessuna posizione finale del log delle transazioni restituita dal server
 %s: "%s%s" non rinominato, il segmento non è completo
 %s: la vecchia directory non è un percorso assoluto nella mappatura dei tablespace: %s
 %s: solo i backup in modalità tar possono essere compressi
 %s: memoria esaurita
 %s: ricevuto segnale di interruzione, in uscita
 %s: ricevuti record di log delle transazioni per offset %u senza alcun file aperto
 %s: lo stream di replica è terminato prima del punto di arresto
 %s: il file di segmento "%s" ha la dimensione non corretta %d, saltato
 %s: select() fallita: %s
 %s: il server ha riportato un nome di file della storia imprevisto per la timeline %u: %s
 %s: il server ha riportato la timeline successiva imprevista %u, a seguito della timeline %u
 %s: il server ha restituito una risposta imprevista al comando BASE_BACKUP; ricevute %d righe e %d campi, attese %d righe e %d campi
 %s: il server ha interrotto lo streaming della timeline %u a %X/%X, ma ha riportato l'inizio della timeline successiva %u a %X/%X
 %s: avvio del ricevitore dei WAL in background
 %s: inizio dello streaming dei log a %X/%X (slot %s)
 %s: avvio dello streaming dei log a %X/%X (timeline %u)
 %s: la timeline di inizio %u non è presente nel server
 %s: intestazione dello streaming troppo piccola: %d
 %s: streaming iniziato
 %s: passato alla timeline %u a %X/%X
 %s: questa piattaforma non supporta i link simbolici
 %s: l'identificativo di sistema non combacia tra il backup di base e la connessione in streaming
 %s: questo binario compilato non supporta la compressione
 %s: troppi argomenti nella riga di comando (il primo è "%s")
 %s: la posizione della directory del log delle transazioni può essere specificata solo in modalità plain
 %s: la posizione della directory del log delle transazioni deve essere un percorso assoluto
 %s: il file di log delle transazioni "%s" ha %d byte, dovrebbero essere 0 or %d
 %s: il transfer rate "%s" eccede l'intervallo degli interi
 %s: il transfer rate "%s" non è un valore valido
 %s: il transfer rate "%s" è fuori dall'intervallo consentito
 %s: il transfer rate deve essere maggiore di zero
 %s: risposta inattesa al comando TIMELINE_HISTORY: ricevute %d righe e %d campi, attese %d righe e %d campi
 %s: risultato imprevisto dopo la fine della timeline: ricevute %d righe e %d campi, attese %d righe e %d campi
 %s: terminazione inaspettata dello stream di replica: %s %s: indicatore di link sconosciuto "%c"
 %s: intestazione dello streaming sconosciuta: "%c"
 %s: in attesa che il processo in background finisca lo streaming ...
 Password:  Prova "%s --help" per maggiori informazioni.
 Utilizzo:
 impossibile duplicare il puntatore nullo (errore interno)
 memoria esaurita
 punto di avvio log delle transazioni: %s sulla timeline %u
 