<?php
namespace app\models;
use yii\base\Model;


class TestForm extends Model
{
  public $DateTimeBegin;
  public $DateTimeEnd;
  public $Name;
  public $Teacher;
  public $Auditorium;
  public $Type;

  public function attributeLabels()
  {
    return[
      'DateTimeBegin' => 'Введите дату и время начала занятия',
      'DateTimeEnd' => 'Введите дату и время окончания занятия',
      'Name' => 'Введите название предмета',
      'Teacher' => 'Введите ФИО преподавателя',
      'Auditorium' => 'Введите аудиторию',
      'Type' => 'Введите тип занятия',
    ];
  }

  public function rules()
  {
    return[
      [['DateTimeBegin', 'DateTimeEnd'], 'safe'],
      ['Name', 'required'],
      ['Auditorium', 'required'],
      ['Teacher', 'required'],
      ['Type', 'number'],
    ];
  }
}

?>
