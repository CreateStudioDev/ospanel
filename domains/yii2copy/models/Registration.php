<?php
namespace app\models;
use yii\base\Model;


class Registration extends Model
{
  public $phoneNumber;
  public $secretCode = 0;

  public function attributeLabels()
  {
    return[
      'phoneNumber' => 'Введите номер телефона',
      'secretCode' => 'Введите код из СМС',
    ];
  }

  public function rules()
  {
    return[
      ['phoneNumber', 'required'],
      ['secretCode', 'required'],
    ];
  }
}

?>
