

<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
?>

<?php
 ?>




<?php $form = ActiveForm::begin() ?>
<?= $form->field($model, 'DateTimeBegin')->textInput(['style' => 'width:300px']); ?>
<?= $form->field($model, 'DateTimeEnd')->textInput(['style' => 'width:300px']); ?>
<?= $form->field($model, 'Name')->textInput(['style' => 'width:200px']); ?>
<?= $form->field($model, 'Teacher')->textInput(['style' => 'width:200px']); ?>
<?= $form->field($model, 'Auditorium')->textInput(['style' => 'width:200px']); ?>
<?= $form->field($model, 'Type')->textInput(['style' => 'width:100px']); ?>
<?= Html::submitButton('Добавить', ['class' => 'btn btn-success']) ?>
<?php ActiveForm::end() ?>

<h1><b><?= Html::encode("На этот день запланировано") ?></b></h1>
  <font size = 5>
    <?php foreach ($date as $d): ?>
      <li>
        <?php
          $dt1 = explode(' ', $d->DateTimeBegin);
          $dtHM = explode(':', $dt1[1]);
          $timeBeginH = $dtHM[0];
          $timeBeginM = $dtHM[1];
          $dt2 = explode(' ', $d->DateTimeEnd);
          $dtHM2 = explode(':', $dt2[1]);
          $timeEndH = $dtHM2[0];
          $timeEndM = $dtHM2[1];
        ?>
        <?= $timeBeginH.":".$timeBeginM."-".$timeEndH.":".$timeEndM." ".$d->Name." |   ".$d->Auditorium." | " ?>
      </li>
    <?php endforeach; ?>
  </font>
