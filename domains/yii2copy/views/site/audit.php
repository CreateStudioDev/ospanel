<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Аудитории';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><b><?= Html::encode("Список аудиторий") ?></b></h1>
      <font size = 5>
        <?php foreach ($data as $d): ?>
          <li>
            <?= $d->Name ?>
          </li>
        <?php endforeach; ?>
      </font>

</div>
