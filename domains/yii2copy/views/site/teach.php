<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Преподаватели';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-teach">
    <h1><b><?= Html::encode("Список преподавателей") ?></b></h1>
      <font size = 5>
        <?php foreach ($data as $d): ?>
          <li>
            <?= $d->Name ?>
          </li>
        <?php endforeach; ?>
      </font>

</div>
