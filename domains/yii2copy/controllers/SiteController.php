<?php

namespace app\controllers;

use app\models\TestForm;
use Yii;
use yii\web\Controller;
use app\models\Timetable;
use app\models\Auditorium;
use app\models\Teacher;
use app\models\Registration;

require_once 'sms.ru.php';

class SiteController extends Controller
{
	//$code = 0;

	public function actionIndex()
	{
		$model = new TestForm();
		if ($model->load(Yii::$app->request->post()))
		{
			if ($model->validate()){
				$dts = explode(' ', $model->DateTimeBegin);
				$current_date = $dts[0];
				$searchingRecord1 = Timetable::find()->where(['<=', 'DateTimeBegin', $model->DateTimeBegin])
													 ->andWhere(['>=', 'DateTimeEnd', $model->DateTimeBegin])
													 ->andWhere(['Auditorium' => $model->Auditorium])
													 ->all();
				$searchingRecord2 = Timetable::find()->where(['<=', 'DateTimeBegin', $model->DateTimeEnd])
													 ->andWhere(['>=', 'DateTimeEnd', $model->DateTimeEnd])
													 ->andWhere(['Auditorium' => $model->Auditorium])
													 ->all();
				$searchingRecord3 = Timetable::find()->where(['>', 'DateTimeBegin', $model->DateTimeBegin])
													 ->andWhere(['<', 'DateTimeEnd', $model->DateTimeEnd])
													 ->andWhere(['Auditorium' => $model->Auditorium])
													 ->all();
				$searchingAudiorium = Auditorium::find()->where(['Name' => $model->Auditorium])->all();
				$searchingTeacher = Teacher::find()->where(['Name' => $model->Teacher])->all();
				$countRecords = count($searchingRecord1) + count($searchingRecord2) + count($searchingRecord3);
				if ($countRecords == 0 && count($searchingAudiorium) == 1 && count($searchingTeacher) == 1){
					Yii::$app->session->setFlash('success', 'Занятие добавлено в расписание!');
					$tempID = new Timetable();
					$tempID->DateTimeBegin = date($model->DateTimeBegin);
					$tempID->DateTimeEnd = date($model->DateTimeEnd);
					$tempID->Name = $model->Name;
					$tempID->Teacher = $model->Teacher;
					$tempID->Auditorium = $model->Auditorium;
					$tempID->Type = $model->Type;
					$tempID->save();
				}else{
					if (count($searchingAudiorium) == 0){
						Yii::$app->session->setFlash('error', 'Данной аудитории нет в списке!');
					}else {
						Yii::$app->session->setFlash('error', 'Аудитория занята!');
					}
					if (count($searchingTeacher) == 0){
						Yii::$app->session->setFlash('error', 'Данного преподавателя нет в списке!');
					}
				}

			}else{
				Yii::$app->session->setFlash('error', 'Данные некорректны');
			}

		}
		$date = Timetable::find()->where(['DATE(DateTimeBegin)' => $current_date])->all();
		//$leftDate = new DateTime(date($model->DateTimeBegin))
		//$data = Timetable::find()->where(['>', 'DateTimeBegin', date($model->DateTimeBegin) + strtotime('00:00:00'))])
			//											 ->andWhere(['<', 'DateTimeBegin', datetime(date($model->DateTimeBegin) + strtotime('23:59:59'))])->all();
		return $this->render('index', compact('model', 'date'));
	}

	public function actionAudit()
	{
			$data = Auditorium::find()->all();
			return $this->render('audit', compact('data'));
	}

	public function actionTeach()
	{
			$data = Teacher::find()->all();
			return $this->render('teach', compact('data'));
	}

	public function actionSender()
	{
		$model = new Registration();

		if ($model->load(Yii::$app->request->post()))
		{
				$number = $model->phoneNumber;

				$body = file_get_contents("https://sms.ru/sms/send?api_id=E511FBD0-FC67-5FF1-1893-8806473B2469&to=".$number."&msg=".urlencode($code)."&json=1");
				$json = json_decode($body);
				if ($model->secretCode == $code){
					Yii::$app->session->setFlash('success', 'Код верный!');
				}else {
					Yii::$app->session->setFlash('error', 'Код неверный!');
				}
		}
		return $this->render('sender', compact('model'));
	}

}
?>
