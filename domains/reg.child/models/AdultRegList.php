<?php
namespace app\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class AdultRegList extends Model
{
    public $phoneNumber;
    public $name;
    public $surname;
    public $workPlace;

    public function rules()
    {
        return [
            ['phoneNumber', 'match', 'pattern' => '/^\+7\-[0-9]{3}\-[0-9]{3}\-[0-9]{2}\-[0-9]{2}$/'],
            [['name', 'surname', 'workPlace'], 'string'],
        ];
    }
}
?>
