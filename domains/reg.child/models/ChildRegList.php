<?php
namespace app\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class ChildRegList extends Model
{
    public $phoneNumber;
    public $name;
    public $surname;
    public $studyPlace;
    public $age;
    public $class;

    public function rules()
    {
        return [
            ['phoneNumber', 'match', 'pattern' => '/^\+7\-[0-9]{3}\-[0-9]{3}\-[0-9]{2}\-[0-9]{2}$/'],
            [['name', 'surname', 'studyPlace'], 'string'],
            ['age', 'validateUserBirthdate'],
            ['class', ['validateUserClass', 'int']],
        ];
    }

    public function valiateUserBirthdate($attribute, $params)
    {
        $date = date('d.m.Y');
        $minAgeDate = date('d.m.Y', strtotime('-7 year'));
        $maxAgeDate = date('d.m.Y', strtotime('15 year'));
        if ($this->$attribute > $minAgeDate) {
            $this->addError($attribute, 'Дата очень раняя');
        } elseif ($this->$attribute < $maxAgeDate) {
            $this->addError($attribute, 'Дата очень поздняя');
        }
    }

    public function validateUserClass($attribute, $params)
    {
        if ($this->$attribute < 2 || $this->attribute > 10)
        {
            this->addError($attribute, 'Неверно указан класс!');
        }
    }

}
?>
