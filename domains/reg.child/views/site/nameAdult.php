<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\Pjax;
use kartik\alert\Alert;


?>


        <?php Pjax::begin(['enablePushState' => false]); ?>

        <!-- $form = ActiveForm::beginForm(['site/register-on-event'], 'post', ['data-pjax' => '', 'class' => 'form-inline']); -->
        <?php $form = ActiveForm::begin(['site/name-adult'], 'options' => ['data' => ['pjax' => true]]]); ?>

        <?= $form->field($modelAdult, 'name')->textInput() ?>

        <?= Html::submitButton('Далее', ['class' => 'btn btn-lg btn-primary', 'id' => 'send', 'name' => 'hash-button']) ?>
        <?php ActiveForm::end(); ?>


<?php Pjax::end(); ?>
