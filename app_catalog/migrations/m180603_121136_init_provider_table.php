<?php

use yii\db\Migration;

/**
 * Class m180603_121136_init_provider_table
 */
class m180603_121136_init_provider_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
      $this->createTable(
          'providerT',[
            'id' => 'pk',
            'id_product' => 'int unique',
            'name' => 'string',
          ],
          'ENGINE=InnoDB'
        );

        $this->addForeignKey('product_provider', 'providerT', 'id_product',
                            'product', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('product_provider', 'provider');
        $this->dropTable('provider');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180603_121136_init_provider_table cannot be reverted.\n";

        return false;
    }
    */
}
