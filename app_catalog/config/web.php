<?php
return[
	'id' => 'app_catalog',
	'basePath' => realpath(__DIR__ . '/../'),
	'components' => [
		'request' => [
			'cookieValidationKey' => '123',
		],
		'db' => require(__DIR__ . '/db.php'),
	]
];
?>
