<?php

use yii\db\Migration;

/**
 * Class m180603_112919_init_timetable
 */
class m180603_112919_init_timetable extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
      $this->createTable(
        'product',[
          'id' => 'pk',
          'name' => 'string',
          'price' => 'double',
          'description' => 'text',
        ],
        'ENGINE=InnoDB'
      );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
      $this->dropTable('product');
      //  echo "m180603_112919_init_timetable cannot be reverted.\n";

      //  return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180603_112919_init_timetable cannot be reverted.\n";

        return false;
    }
    */
}
